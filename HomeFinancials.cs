﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;

namespace MobileSite

{ 

    public class HomeFinancials
    {

        public static Double MortgageEstimate{ get; set; }

       public static Double GetMonthlyMortgagePayment(Double Rate, Double DownPayment, Double PurchaseAmount, Double PropertyTax, Double PropertyInsurance, int LoanTerm) 
        {

      

            try
            {
                Int32 ps = Convert.ToInt32(PurchaseAmount);
                Int32 dp = Convert.ToInt32(DownPayment);

                MortgageEstimate = 0;
        Double pDown = ps  * dp / 100;
        Double result = 0;

        Double mTerm = LoanTerm * 12;

        Double r = Rate / (12 * 100);

        Double re = (Double)Math.Pow((1 + r), mTerm);

        result = ((PurchaseAmount - pDown) * r * re / (re - 1)) + (PropertyTax / 12) + (PropertyInsurance / 12);

        MortgageEstimate = result;
        return result;

            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return 0;
            }



       

        }


    }
}