﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.master" CodeBehind="listingDetail.aspx.cs" Inherits="MobileSite.listingDetail" %>
<%@ Register Src="/Controls/MortgageCalculator.ascx" TagName="MC" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">



        <div style="width:280px;text-align:center;">
    <asp:Label ID="listingInfoTop"  ClientIDMode="Static" CssClass="listingDetailContainer" runat="server"></asp:Label> 
                <asp:Label ID="mEstLabel" runat="server" Text="$0" CssClass="listingDetailContainer"></asp:Label>
              <asp:Label ID="listingInfoBot"  CssClass="listingDetailContainer" runat="server"></asp:Label> 
    <asp:Panel ID="imgs" runat="server">
        <div class="clear"></div>
        <div id="imgContainer">
            <div id="imagegallery">
                <asp:Literal ID="listingImages" runat="server"></asp:Literal>
            </div>
           <!-- <a id="prev" href="#"><span class="P-arrow previous"></span></a><a id="next" href="#"><span class="N-arrow next"></span></a> //-->
        </div>
        <div id="captions"><asp:Literal ID="initialCaption" runat="server"></asp:Literal></div>
    </asp:Panel>
      </div>

      <div class="clear"></div>

       <form id="Form1" runat="server">

        <asp:Panel ID="FPDD" runat="server">
            <a href="Javascript:opPanel('#outerFpContainer', '#fpImg')" style="text-decoration:none;">
                <div class="listingMenuItem" id="fpBtn">View Floor Plan<img id="fpImg" src="http://1627.mobimanage.com/IMAGES/triangle.png" style="float:right;margin-right:10px;padding-top:5px;" width="7" height="11" /></div>
            </a>

            <div id="outerFpContainer" class="detailDropDown" style="display: none;">
   
                <asp:Literal ID="FPList" runat="server"></asp:Literal>
      
            </div>
        </asp:Panel>
  

        <%--- Community Info ---%>
        <a href="Javascript:opPanel('#comm_info', '#mirImg')" style="text-decoration:none;">
            <div class="listingMenuItem" id="MIRBtn" >Call Now<img id="mirImg" src="http://1627.mobimanage.com/IMAGES/triangle.png" style="float:right;margin-right:10px;padding-top:5px;" width="7" height="11" /></div>
        </a>

        <div id="comm_info" class="detailDropDown" style="display: none;">
    <asp:Literal ID="comm_info_l" runat="server"></asp:Literal>

        </div>




     <%--- Mortgage Info ---%>
        <a href="Javascript:opPanel('#MCPanel', '#mcImg')" style="text-decoration:none;">
            <div class="listingMenuItem" id="schoolBtn" >Mortgage Calculator<img id="mcImg" src="http://1627.mobimanage.com/IMAGES/triangle.png" style="float:right;margin-right:10px;padding-top:5px;" width="7" height="11" /></div>
        </a>

        <div id="MCPanel" class="detailDropDown" style="display: none;">
            <uc1:MC ID="calc" runat="server" visible="true" />
        </div>


  
           <%--- SHARE---%>
        <a href="Javascript:opPanel('#share_panel', '#shareImg')" style="text-decoration:none;">
            <div class="listingMenuItem" id="dirBtn" >Share This Home<img id="shareImg" src="http://1627.mobimanage.com/IMAGES/triangle.png" style="float:right;margin-right:10px;padding-top:5px;" width="7" height="11" /></div>
        </a>

            <div id="share_panel" class="detailDropDown" style="display: none;">
   
            <asp:Literal ID="share_comm" runat="server"></asp:Literal>
           
        </div>
  
                   </form>  



</asp:Content>

