﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.Master" CodeBehind="photos.aspx.cs" Inherits="MobileSite.photos" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">



<asp:Panel ID="bbOS" runat="server">
    <script type="text/javascript">
        var ua = navigator.userAgent;
        if (ua.indexOf("BlackBerry") >= 0) {
            if (ua.indexOf("Version/") >= 0) { // ***User Agent in BlackBerry 6 and BlackBerry 7
                Verposition = ua.indexOf("Version/") + 8;
                TotLenght = ua.length;

            }
            else {// ***User Agent in BlackBerry Device Software 4.2 to 5.0
                var SplitUA = ua.split("/");
                window.location = '/photos.aspx?os5=1'
            }
        }
</script>

</asp:Panel>

<div class="content">

  <h2 class="detailView">PHOTOS</h2>

  <asp:Literal ID="galLabel" runat="server"></asp:Literal>

</div>

<asp:Panel ID="slideJS" runat="server">


<script type="text/javascript"><!--
    $(document).ready(function () {

        $('.iAmImg').each(function () {
            var img_width = $(this).width();
            var img_height = $(this).height();


            var modifier = 270 / img_width;
            var new_height = img_height * modifier;

            //alert("My width:" + img_width + "px, My Height: " + img_height + "px<br />mod:" + modifier + " new height:" + new_height);

            $(this).width(270);
            $(this).height(Math.floor(new_height));

        });

        $('#imagegallery').cycle({
            timeout: 0,
            fx: 'scrollHorz',
            next: '#next',
            prev: '#prev',
            speed: 200
        });

        $("#imagegallery").touchwipe({
            wipeLeft: function () {
                $("#imagegallery").cycle("next");
            },
            wipeRight: function () {
                $("#imagegallery").cycle("prev");
            }
        });
    });
    $(window).load(function () {


    });
// --></script>

</asp:Panel>

</asp:Content>
