﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MobileSite
{
    public partial class add : System.Web.UI.Page
    {




        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String listingTable = Utility.GetMobiSiteSettings("ListingsTable");
        protected String accountID = "10048"; //System.Configuration.ConfigurationManager.AppSettings["accountID"].ToString();
        int alt = 0;
        protected String type = "";
        protected int listID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["listID"] != null)
                    listID = int.Parse(Request.QueryString["listID"]);
                if (Request.QueryString["type"] != null)
                    type = Request.QueryString["type"].ToString();
                DisplayData();
            }
        }

        protected void DisplayData()
        {
            // If cookie is empty and user is just viewing
            if (Request.Cookies["community"] != null && Request.Cookies["plans"] != null && Request.Cookies["MIRHomes"] != null)
            {
                FavDisplay.Text = "<div class='content' style='margin-top: -8px;'>There are currently no items in your favorite list.<br /><br />To add listings to your favorites click on the " +
                "<div class=\"smallBtn\" style=\"display:inline-block;\">Save</div> button.<br /><br />If an item has already been added to you favorites than it will appear " +
                "<div class=\"smallBtn\" style=\"background-color:#660000; color: #FFF; display:inline-block;\">Saved</div>.<br /><br />You can add as many favorite items as you'd like!</div>";
                return;

            }
            else
            {
                // handle communities
                if (Request.Cookies["community"] != null)
                {
                    String favList = Server.UrlDecode(Request.Cookies["community"].Value);

                    if (favList.Length > 0)
                    {

                        FavDisplay.Text += "<div class=\"pageDataNavContainer\">Communities</div>";

                        String commQuery = "SELECT DISTINCT SubDivisionName, SubdivisionID, SalesOffice_Street1, SalesOffice_City, SubCity, SubState, SubZip "
                 + "FROM BHI_Subdivision s JOIN BHI_Builder b ON b.BuilderID=s.BuilderID "
                 + "WHERE s.SubDivisionID IN (" + favList + ") AND B.BUILDERID IN (SELECT DISTINCT BuilderID FROM Builder " 
                 + "WHERE CORPORATIONID IN (SELECT DISTINCT CORPORATIONID "
                 + "FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101'))";

                        DataSet commListing = SqlHelper.ExecuteDataset(connStr, CommandType.Text, commQuery);

                        foreach (DataRow drTemp in commListing.Tables[0].Rows)
                        {
                            alt += 1;

                            if (alt % 2 == 0)
                            {
                                FavDisplay.Text += "<div class=\"listingContainer\"";
                            }
                            else
                            {
                                FavDisplay.Text += "<div class=\"listingContainerAlt\"";
                            }

                            FavDisplay.Text += " style=\"padding-top: 0px; min-height: 110px;border-bottom: 2px solid #666;\"><a href=\"/communityDetail.aspx?listID=" +
                                drTemp["SubDivisionID"].ToString() + "\" style=\"color: #000; text-decoration: none; font-weight: normal;\"><div style=\"width: 90%; float: left; padding-top:10px;\">" +
                                "<h2 style=\"margin-left:-4px;\">" + drTemp["SubDivisionName"].ToString() + "</h2>" + drTemp["SalesOffice_Street1"].ToString() + "<br /> " + drTemp["SubCity"].ToString() + " " +
                                drTemp["SubZip"].ToString() + "</div></a><div style=\"width:10%; float:left;text-align:center;\"><a href=\"Javascript:removeFav('" + drTemp["SubDivisionID"].ToString() +
                                "', 'community')\"><div class=\"submitbutton\" style='float: right;margin-top: 35px;width:15px'>X</div></a></div><br /><br /></div>\n<div class='clear'></div>\n\n";
                        }
                    }

                }

                //    String Query = "SELECT DISTINCT listingID, Company, address1, city, state, zip, image_list, phone, website, email, description, latitude, longitude FROM " + listingTable + " WHERE ACCOUNTID = " + accountID +
                //        " AND listingID IN(" + favList + ") ORDER BY Company";

                //    DataSet getListing = SqlHelper.ExecuteDataset(connStr, CommandType.Text, Query);

                //    foreach (DataRow drTemp in getListing.Tables[0].Rows)
                //    {
                //        String image = drTemp["image_list"].ToString().Split('|')[0];

                //        FavDisplay.Text += "<div class='listingContainer' style='min-height: 110px;'><div style=\"float: left; height: 90px; margin-right: 10px; text-align: center; vertical-align: bottom; padding-top: 15px; width: 125px;\">" +
                //            "<img src=\"" + image + "\" style=\"vertical-align: top; max-height: 95px; width: 90px;\" /></div><a href='/add.aspx?type=remove&listid=" + drTemp["listingID"].ToString() + 
                //            "'><img src='http://lib.mobimanage.com/ICONS/remove.png' style='float: right;' /></a>" +
                //            "<a href='/listingDetail.aspx?CNM=&listID=" + drTemp["listingID"].ToString() + "'>" + drTemp["Company"].ToString() +
                //            "</a><br />" + drTemp["Address1"].ToString() + "<br /> " + drTemp["City"].ToString() + ", " + drTemp["state"].ToString() + " " + drTemp["zip"].ToString() +
                //            "<br /><br /></div>\n<div class='clear'></div>\n\n";
                //    }

                //}

                // handle floor plans

                if (Request.Cookies["plan"] != null)
                {
                    String favList = Server.UrlDecode(Request.Cookies["plan"].Value);

                    if (favList.Length > 0)
                    {

                        FavDisplay.Text += "<div class=\"pageDataNavContainer\">Floor Plans</div>";

                        String commQuery = "SELECT DISTINCT [PlanName],s.SubdivisionID, d.Bedrooms, d.BaseSqft, d.Baths, d.Garage, d.PlanID, s.SubDivisionName, d.BasePrice "
                                  + "FROM BHI_SubDivision s JOIN BHI_Builder b ON b.BuilderID=s.BuilderID LEFT JOIN BHI_Plans d ON d.SubDivisionID=s.SubDivisionID "
                                  + "WHERE d.PlanID IN (" + favList + ") AND B.BUILDERID IN (SELECT DISTINCT BuilderID FROM Builder "
                                  + "WHERE CORPORATIONID IN (SELECT DISTINCT CORPORATIONID "
                                  + "FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101'))";

                        DataSet commListing = SqlHelper.ExecuteDataset(connStr, CommandType.Text, commQuery);
                       
                        foreach (DataRow drTemp in commListing.Tables[0].Rows)
                        {
                            alt +=1;

                            if (alt % 2 == 0)
                            {
                             FavDisplay.Text += "<div class=\"listingContainer\"";
                            }
                            else
                            {
                             FavDisplay.Text += "<div class=\"listingContainerAlt\"";
                            }





                            FavDisplay.Text += " style=\"padding-top: 0px; min-height: 110px;border-bottom: 2px solid #666;\"><a href=\"/FloorDetail.aspx?listID=" + drTemp["PlanID"].ToString() +
                                 "\" style=\"color: #000; text-decoration: none; font-weight: normal;\"><div style=\"width: 90%; float: left;padding-top:10px;\">" +
                                 "<h2 style=\"margin-left:-4px;\">" + drTemp["SubDivisionName"].ToString() + " - " + drTemp["PlanName"] + "</h2>" +
                                 "Starting at $" + drTemp["BasePrice"].ToString().Substring(0, 3) + "K <br />"
                                + drTemp["Bedrooms"].ToString() + " Bed " + drTemp["Baths"].ToString() + " Bath " + double.Parse(drTemp["Garage"].ToString()).ToString("#0") + " Garage "
                                + drTemp["BaseSqft"].ToString() + " Sq Ft "
                                +  "</div></a><div style=\"float:left;text-align:center;width:10% \"><a href=\"Javascript:removeFav('" + drTemp["PlanID"].ToString() +
                                "', 'plans') title=\"Remove\"><div class=\"submitbutton\" style=\"float: right;margin-top: 35px;width:15px\">X</div></a></div><br /><br /></div>\n<div class='clear'></div>\n\n";

                        }
                    }

                }

                // handle move in ready

                if (Request.Cookies["MIRHouse"] != null)
                {
                    String favList = Server.UrlDecode(Request.Cookies["MIRHouse"].Value);

                    if (favList.Length > 0)
                    {

                        FavDisplay.Text += "<div class=\"pageDataNavContainer\">Move In Ready Homes</div>";

                        String commQuery = "SELECT DISTINCT SpecStreet1,s.SubdivisionID,d.SpecCity,c.PlanID,d.SpecPrice,d.SpecID,c.PlanName "
                                   + "FROM BHI_SubDivision s JOIN BHI_Builder b ON b.BuilderID=s.BuilderID JOIN BHI_Plans c ON c.SubDivisionID=s.SubDivisionID LEFT JOIN BHI_Specs d ON d.PlanID=c.PlanID "
                                   + "WHERE d.SpecID IN (" + favList + ") AND B.BUILDERID IN (SELECT DISTINCT BuilderID FROM Builder "
                                   + "WHERE CORPORATIONID IN (SELECT DISTINCT CORPORATIONID "
                                   + "FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101'))";

                        DataSet commListing = SqlHelper.ExecuteDataset(connStr, CommandType.Text, commQuery);

                        foreach (DataRow drTemp in commListing.Tables[0].Rows)
                        {



                            alt += 1;

                            if (alt % 2 == 0)
                            {
                                FavDisplay.Text += "<div class=\"listingContainer\"";
                            }
                            else
                            {
                                FavDisplay.Text += "<div class=\"listingContainerAlt\"";
                            }


                            FavDisplay.Text += " style=\"padding-top: 0px; min-height: 110px;border-bottom: 2px solid #666;\"><a href=\"/listingDetail.aspx?listID=" + drTemp["SpecID"].ToString() +
                                "\" style=\"color: #000; text-decoration: none; font-weight: normal;\"><div style=\"width: 90%; float: left; padding-top:10px;\">" +
                                "<h2 style=\"margin-left:-4px\">" + drTemp["SubDivisionID"].ToString() + " - " + drTemp["PlanName"] + "</h2>"
                                + drTemp["SpecStreet1"].ToString() + "<br /> " + drTemp["SpecCity"].ToString() + "<br />$" + double.Parse(drTemp["SpecPrice"].ToString()).ToString("#00,000") +
                                "K </div></a><div style=\"width:10%; float:left;text-align:center;\"><a href=\"Javascript:removeFav('" + drTemp["SpecID"].ToString() +
                                "', 'MIRHouse')\"><div class=\"submitbutton\" style='float: right;margin-top: 35px;width:15px;'>X</div></a></div><br /><br /></div>\n<div class='clear'></div>\n\n";

                        }
                    }

                }


            }
        }

        protected String listOut(String[] array)
        {
            String output = "";
            for (int i = 0; i < array.Length; i++)
            {
                if (!String.IsNullOrEmpty(array[i]))
                {
                    output += array[i] + ",";
                }
            }

            if (output.Length > 0)
            {
                if (output.Substring(output.Length - 1) == ",")
                {
                    return output.Substring(0, output.Length - 1);
                }
            }

            return output;
        }






    }
}