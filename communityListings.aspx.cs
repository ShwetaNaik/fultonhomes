﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MobileSite
{
    public partial class communityListings : System.Web.UI.Page
    {
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String listingTable = "FULTON_Comunities";
        protected int listingsPerPage = 20;
        int sRow = 0;
        int pageCount = 20;
        int TotalPages = 0;
        Double txtlat = 0;
        Double txtlon = 0;
        

   
     
        String SubCity="";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["showMap"] = true;

       if (Request.QueryString["txtlat"] !=null)
           txtlat = Convert.ToDouble(Request.QueryString["txtlat"]);
       if (Request.QueryString["txtlon"] != null)
           txtlon= Convert.ToDouble(Request.QueryString["txtlon"]);


                if (Request.QueryString["sRow"] != null)
                    sRow = int.Parse(Request.QueryString["sRow"].Split(',').Last());

                if (Request.QueryString["pg"] != null)
                    pageCount = Convert.ToInt32(Request.QueryString["pg"].ToString());

                if (Request.QueryString["city"] !=null)
                {

                   SubCity = Request.QueryString["city"];

                }
                else
                {
                    if  (Request.QueryString["txtlat"] ==null)
                    Response.Redirect("search_bylocation.aspx");


                }


        

          

              DisplayData();




            }
        }


        protected string lastCity = "";
        protected string GroupCity(string city) {
            var last = lastCity;
            lastCity = city;
            if (last != city) {
                return string.Format("<div class='communityGroup'>{0} Communities</div>", city);
            }
            return "";
        }



        public int getPage()
        {

            return pageCount;


        }


        private void DisplayData()
        {
        

            // Query for listing data

            String aSQL = "";



            if (txtlat != 0)
            {
                String cSQL = " ((ACOS(  (SIN(SalesOffice_latitude * PI() / 180) * SIN(" + txtlat + " * PI() / 180)) + ( COS (SalesOffice_Latitude * PI() / 180) * COS(" + txtlat + " * PI() / 180) * COS((SalesOffice_Longitude - " + txtlon +
                    ") * PI() / 180) ) ) * 180 / PI() ) * 69.09) as Dist ";

                aSQL = "SELECT DISTINCT TOP (" + pageCount + ") s.SubDivisionName,s.SubdivisionID,s.SubDivisionNumber, d.Value,PriceLow,SalesOffice_City,SubCity,SubState,SubZip,SubStreet1, SubState, " + cSQL.ToString() 
              + "FROM BHI_Subdivision s JOIN BHI_Builder b ON b.BuilderID=s.BuilderID LEFT JOIN BHI_SubImage d ON d.SubDivisionID=s.SubDivisionID "
              + "WHERE B.BUILDERID IN (SELECT DISTINCT BuilderID FROM Builder "
              + "WHERE CORPORATIONID IN (SELECT DISTINCT CORPORATIONID "
              + "FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101'))";

            }
            else
            {

                aSQL = "SELECT DISTINCT TOP (" + pageCount + ") s.SubDivisionName,s.SubdivisionID,s.SubDivisionNumber,d.Value,PriceLow,SalesOffice_City,SubCity,SubState,SubZip,SubStreet1, SubState, '0' as Dist "
             + "FROM BHI_Subdivision s JOIN BHI_Builder b ON b.BuilderID=s.BuilderID LEFT JOIN BHI_SubImage d ON d.SubDivisionID=s.SubDivisionID "
             + "WHERE SubCity='" + SubCity + "' AND B.BUILDERID IN (SELECT DISTINCT BuilderID FROM Builder "
             + "WHERE CORPORATIONID IN (SELECT DISTINCT CORPORATIONID "
             + "FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101'))";

            }


            if ((txtlat !=0) && (Request.QueryString["sort"] == null))
            {
                aSQL += " ORDER BY Dist,PriceLow ";
            }

            if (Request.QueryString["sort"] !=null)
              
                {
                    if (Convert.ToInt32(Request.QueryString["sort"]) == 0)
                        aSQL += " ORDER BY PriceLow ASC ";
                    else
                        aSQL += " ORDER BY PriceLow DESC ";

                }




            DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());

            // create a paging system for the data repeater
            PagedDataSource pageable = new PagedDataSource();
            pageable.DataSource = aListingsDS.Tables[0].DefaultView;

            //turn on paging
            pageable.AllowPaging = true;
            //set the start row to a sRow (can be passed in URL)
            pageable.CurrentPageIndex = sRow;
            //set listings per page (set in config)
            pageable.PageSize = pageCount;
            TotalPages = pageable.PageCount;


            if (pageCount <= pageable.Count)
            {
              
                if (txtlat !=0)
                    this.commBtn.Text = "<a name=\"target\" href=\"CommunityListings.aspx?txtlat=" + txtlat + "&txtlon=" + txtlon + "&pg=" + (pageCount + listingsPerPage) + "\" style=\"text-decoration:none;\"/><div id=\"comm_btn\" class=\"loadBtn\">Load More Communities</div></a>";
                else
                    this.commBtn.Text = "<a name=\"target\" href=\"CommunityListings.aspx?city=" + aListingsDS.Tables[0].Rows[0]["SubCity"] + "&pg=" + (pageCount + listingsPerPage) + "\" style=\"text-decoration:none;\"/><div id=\"comm_btn\" class=\"loadBtn\">Load More Communities</div></a>";
                
            }
            else
            {
                this.commBtn.Text = "<div id=\"comm_btn\" class=\"loadBtn\" style=\"background-color:#f8f8f8;cursor:default;color:#666;display:none;\">Completed</div>";
            }
            repGetlist.DataSource = pageable;
            repGetlist.DataBind();












            // Set Page number display
            int currPage = sRow + 1;
            //            pageTotal.Text = "PAGE " + currPage + " OF " + pageable.PageCount;

            //Previous and Next links
            //            div1.Text = " | ";
            //div2.Text = " | ";
            if (sRow == 0)
            {
                //                prevBtn.Visible = false;
                //prevBtn2.Visible = false;
                //                div1.Visible = false;
                //div2.Visible = false;
            }
            else
            {
                int prevRow = sRow - 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "&sRow=[0-9]", "", RegexOptions.IgnoreCase);
                //               prevBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
                //prevBtn2.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
            }
            if (pageable.IsLastPage)
            {
                //              nextBtn.Visible = false;
           //   loadComm.Visible = true;


            //    divShowMore.Visible = true;
                //              div1.Visible = false;
                //div2.Visible = false;
            }
            else
            {
                int nextRow = sRow + 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "&sRow=[0-9]", "", RegexOptions.IgnoreCase);
                //                nextBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
              //  loadComm.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
            }

        }





        protected void loadMore(object sender, EventArgs e)
        {

            if (pageCount <= TotalPages)
            {

                Response.AppendHeader("pageCount", "pageCount");
                pageCount += 10;

                DisplayData();


            }

       

        }



        public String checkLen(String input, String type)
        {
            switch (type.ToLower())
            {
                case "address":
                    if (input.Length > 0)
                        return input + "<br />";
                    else
                        return "";
                case "zip":
                    if (input.Length > 0)
                        return input + ",";
                    else
                        break;
                case "image":
                    if (input.Length > 0)
                    {
                        if (!input.Contains("http://"))
                            return "<img src='/IMAGES/" + input + "'style='vertical-align: top; max-height: 95px; width: 90px;' />";
                        else
                            return "<img src='" + input + "'style='vertical-align: top; max-height: 95px; width: 90px;' />";
                    }
                    else
                        return "";
                case "description":
                    if (input.Length > 0)
                        return String.Format("<a href=\"/listingDetail.aspx?CNM={0}&listID=<%# Eval(\"listingID\") %>\"><%# Eval(\"Company\") %></a><br />", Request.QueryString["CNM"].ToString());
                    else
                        return "<%# Eval(\"Company\") %><br />";
            }


            return input;
        }









        }

        

    }
