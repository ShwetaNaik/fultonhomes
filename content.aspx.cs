﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MobileSite
{
    public partial class content : System.Web.UI.Page
    {
        protected int pageID;
        protected static String strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["DBConn"].ToString();
        protected String aDomainID = System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (Request.QueryString["txtlat"] != null && Request.QueryString["txtlon"] != null)
                {

                    Session["txtlat"] = Request.QueryString["txtlat"];
                    Session["txtlon"] = Request.QueryString["txtlon"];
                }


                DisplayData();
            }
        }

        protected void DisplayData()
        {
            //Verify that pageID was passed in
            if (Request.QueryString["pageID"] != null)
            {
                pageID = int.Parse(Request.QueryString["pageID"]);
                Session["PageID"] = pageID;
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }

            // Double-check for page existance and redirect
            DataSet getPage = SqlHelper.ExecuteDataset(strConnection, CommandType.Text, "SELECT pageID, pageData, linkName, rsslink, rssType, sortOrder, pageType, pageRedirect FROM mobiPages WHERE domainID =" 
                + aDomainID + " AND pageID =" + pageID);

            if (getPage.Tables[0].Rows.Count == 0)
            {
                Response.Redirect("/Default.aspx");
            }

            DataRow drPageTemp = getPage.Tables[0].Rows[0];
            if (int.Parse(drPageTemp["pageType"].ToString()) == 1)
            {
                String redir = drPageTemp["pageRedirect"].ToString();
                if (redir.Contains("?")) redir += "&pageID=" + drPageTemp["pageID"].ToString();
                else redir += "?pageID=" + drPageTemp["pageID"].ToString();
                Response.Redirect(redir);
            }


            // Get Subpages
            DataSet getSubPages = SqlHelper.ExecuteDataset(strConnection, CommandType.Text, "SELECT pageID, linkName, sortOrder, pageType, pageRedirect FROM mobiPages WHERE domainID =" 
                + aDomainID + " AND parentPageID =" + pageID + " AND Active = 1 ORDER BY sortOrder");

            foreach (DataRow drSubpagesTemp in getSubPages.Tables[0].Rows)
            {
                if (int.Parse(drSubpagesTemp["pageType"].ToString()) == 1)
                {
                    String redir = drSubpagesTemp["pageRedirect"].ToString();
                    if (redir.Contains("?")) redir += "&pageID=" + drSubpagesTemp["pageID"].ToString();
                    else redir += "?pageID=" + drSubpagesTemp["pageID"].ToString();

                    subPages.Text = subPages.Text + "<a href='" + redir +
                        "' style='text-decoration: none;'><div class='listingMenuItem'>" + drSubpagesTemp["linkName"].ToString().ToUpper() + "<img alt=\"\" id=\"fpImg\" src=\"http://1627.mobimanage.com/IMAGES/triangle.png\" style=\"float:right;margin-right:10px;padding-top:5px;\"></div></a>";
                }
                else
                {
                    subPages.Text = subPages.Text + "<a href='content.aspx?pageID=" + drSubpagesTemp["pageID"].ToString() +
                        "' style='text-decoration: none;'><div class='listingMenuItem'>" + drSubpagesTemp["linkName"].ToString().ToUpper() + "<img src=\"http://1627.mobimanage.com/IMAGES/triangle.png\" style=\"float:right;margin-right:10px;padding-top:5px;\" /></div></a>";
                }
            }

            if (int.Parse(drPageTemp["rssType"].ToString()) == 3 || int.Parse(drPageTemp["rssType"].ToString()) == 1 || 
                (drPageTemp["pageData"] != null && drPageTemp["pageData"].ToString().Length > 0))
            {

                startCdiv.Text = "<div class=\"content\">";
                endCdiv.Text = "</div>";
            }

            // Enable Twitter feed if type 3
            if (int.Parse(drPageTemp["rssType"].ToString()) == 3)
                twit1.Visible = true;

            if (int.Parse(drPageTemp["rssType"].ToString()) == 1)
            {
                uclRSS.Visible = true;
                uclRSS.DisplayData(drPageTemp["rssLink"].ToString());
            }

            //Get Page Content
            if (drPageTemp["pageData"] != null && drPageTemp["pageData"].ToString().Length > 0)
            {
                //Get Page Content
                if (drPageTemp["pageData"] != null)
                    pageData.Text = drPageTemp["pageData"].ToString();
            }

            //headline.InnerText = drPageTemp["linkName"].ToString();

        }
    }
}