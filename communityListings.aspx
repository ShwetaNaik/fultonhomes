﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.Master" CodeBehind="communityListings.aspx.cs" Inherits="MobileSite.communityListings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">
      
        <%--- Prev/next and page total ---%>
<%--        <div class="clear"></div>
        <div class="pageDataNavContainer">
            <asp:Label ID="pageTotal" runat="server"></asp:Label>
            <div class="pageDataNav">
            <asp:HyperLink ID="prevBtn" runat="server">Prev</asp:HyperLink><asp:Label ID="div1" runat="server"></asp:Label><asp:HyperLink ID="nextBtn" runat="server">Next</asp:HyperLink>
            </div>
        </div>--%>
        

        <h2 class="title_bar" style="color:#660000;margin:0px;border-bottom:2px solid #b8b8b8;">Select a Community</h2>
        <%--- Main pageable listing repeater ---%>
        <div class="clear"></div>
            <asp:Repeater runAt="Server" id="repGetlist" >
                <ItemTemplate>
                    <a href="/communityDetail.aspx?subDivisionNumber=<%# Eval("SubDivisionNumber") %>" style="color: #000; text-decoration: none;">
                    <div class="listingContainer" style="min-height:78px;">
                        <div style="float: right; height: auto; padding-top: 34px; vertical-align: middle;"><img src="http://lib.mobimanage.com/ICONS/listingArrow.png" alt="" /></div>
                      <div style="float: left; height: auto;  padding:0px 10px;"><%#  checkLen(Eval("Value").ToString(), "image") %></div>
                        <h2 style="margin:0px;"><%# Eval("SubDivisionName")%></h2>
                       <div class="listingDetailContainer">Starting from <%# Eval("PriceLow", "{0:C0}")%><br />
                       <%# Eval("SubStreet1").ToString().Length > 0 ? Eval("SubStreet1") + "<br />" : "" %> <%#Eval("SubState") %>, <%#Eval("SubCity") %> <%#Eval("SubZip") %><br />
                       <%# Eval("Dist").ToString() !="0" ? Convert.ToDouble(Eval("Dist")).ToString("##.##")  + " miles" : "" %> 
                       </div>
               </div></a>
                    <div class="clear"></div>
                </ItemTemplate>                                                                                     
                <AlternatingItemTemplate>
                    <a href="/communityDetail.aspx?subDivisionNumber=<%# Eval("SubDivisionNumber") %>" style="color: #000; text-decoration: none;">
                    <div class="listingContainerAlt"  style="min-height:78px;">
                             <div style="float: right; height: auto; padding-top: 34px; vertical-align: middle;"><img src="http://lib.mobimanage.com/ICONS/listingArrow.png" alt="" /></div>
                          <div style="float: left; height: auto; vertical-align: bottom;padding:0px 10px;"><%# checkLen(Eval("Value").ToString(), "image") %></div>
                        <h2 style="margin:0px;"><%# Eval("SubDivisionName")%></h2>
                        <div class="listingDetailContainer">Starting from <%# Eval("PriceLow", "{0:C0}")%><br />
                        <%# Eval("SubStreet1").ToString().Length > 0 ? Eval("SubStreet1") + "<br />" : "" %> <%#Eval("SubState") %>, <%#Eval("SubCity") %> <%#Eval("SubZip") %><br />
                        <%# Eval("Dist").ToString() !="0" ? Convert.ToDouble(Eval("Dist")).ToString("##.##") + " miles" : "" %> 
                        </div>
                    </div></a>
                    <div class="clear"></div>
                </AlternatingItemTemplate>
            </asp:Repeater> 






                <div class="clear"></div>
                

<form runat="server">  
      <div style="width:100%;text-align:center;">
          <asp:Literal ID="commBtn" runat="server"></asp:Literal>
     </div>
</form>
          

    
</asp:Content>
