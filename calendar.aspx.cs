﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MobileSite
{
    public partial class calendar : System.Web.UI.Page
    {
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String eventsTable = Utility.GetMobiSiteSettings("EventsTable");
        protected int listingsPerPage = Utility.GetMobiSiteSettingsInt("NumListingRows");
        int monthID = DateTime.Now.Month;
        DateTime startDay = new DateTime();
        DateTime endDay = new DateTime();
        int sRow = 0;
        public String ddLink = "";
        int monthOpen = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (Request.QueryString["monthID"] != null)
                    monthID = int.Parse(Request.QueryString["monthID"]);
                if (Request.QueryString["CNM"] != null)
                {
                    Page.Title = Request.QueryString["CNM"];

                }
                if (Request.QueryString["sRow"] != null)
                    sRow = int.Parse(Request.QueryString["sRow"]);
                if (Request.QueryString["monthOpen"] != null)
                    monthOpen = int.Parse(Request.QueryString["monthOpen"]);
                DisplayData();
            }
        }

        private void DisplayData()
        {
            // Handle the by month dropdown
            String ddQuery = Request.QueryString.ToString();
            ddQuery = Regex.Replace(ddQuery, "&monthOpen=[0-9]", "", RegexOptions.IgnoreCase);
            ddQuery = Regex.Replace(ddQuery, "monthOpen=[0-9]", "", RegexOptions.IgnoreCase);
            int next = 1;
            if (monthOpen == 1)
                next = 0;
            ddLink = Request.ServerVariables["URL"] + "?" + ddQuery + "&monthOpen=" + next;

            if (monthOpen == 1)
            {
                jsEn.Visible = false;
                jsDis.Visible = true;
            }

            for (int i = 1; i <= 12; i++)
            {
                monthList.Text += "<a style='text-decoration: none;' href='/calendar.aspx?monthID=" + i + "'><div class='subNavDetail'";
                if (monthID == i)
                    monthList.Text += " style='background: #eae2d7;'";
                monthList.Text += ">" + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i) + "</div></a>";
            }


            // Do date calculations
            int year = DateTime.Now.Year;
            int day = 1;

            if (monthID == DateTime.Now.Month)
                day = DateTime.Now.Day;
            if (monthID < DateTime.Now.Month)
                year++;

            startDay = DateTime.Parse(monthID + "/" + day + "/" + year);
            endDay = startDay.AddMonths(1);
            endDay = endDay.AddDays(-(day));


            String query = String.Format("SELECT DISTINCT eventID, Title, StartDate, EndDate, location, Image_List " +
            "FROM {0} WHERE 1=1 AND (EndDate >= '{1}' AND StartDate <= '{2}' ) ORDER BY StartDate, EndDate"
            , eventsTable, startDay.ToString("MM/dd/yyyy"), endDay.ToString("MM/dd/yyyy"));
            DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, query);

            // create a paging system for the data repeater
            PagedDataSource pageable = new PagedDataSource();
            pageable.DataSource = aListingsDS.Tables[0].DefaultView;

            //turn on paging
            pageable.AllowPaging = true;
            //set the start row to a sRow (can be passed in URL)
            pageable.CurrentPageIndex = sRow;
            //set listings per page (set in config)
            pageable.PageSize = listingsPerPage;

            repGetlist.DataSource = pageable;
            repGetlist.DataBind();

            // Set Page number display
            int currPage = sRow + 1;
            pageTotal.Text = "PAGE " + currPage + " OF " + pageable.PageCount;

            //Previous and Next links
            div1.Text = " | ";
            div2.Text = " | ";
            if (sRow == 0)
            {
                prevBtn.Visible = false;
                prevBtn2.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
            }
            else
            {
                int prevRow = sRow - 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "&sRow=[0-9]", "", RegexOptions.IgnoreCase);
                cleanQuery = Regex.Replace(cleanQuery, "sRow=[0-9]", "", RegexOptions.IgnoreCase);
                prevBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
                prevBtn2.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
            }
            if (pageable.IsLastPage)
            {
                nextBtn.Visible = false;
                nextBtn2.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
            }
            else
            {
                int nextRow = sRow + 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "&sRow=[0-9]", "", RegexOptions.IgnoreCase);
                cleanQuery = Regex.Replace(cleanQuery, "sRow=[0-9]", "", RegexOptions.IgnoreCase);
                nextBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
                nextBtn2.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
            }
           
        }

        public String checkLen(String input, String type)
        {
            if (type.Equals("Address"))
            {
                if (input.Length > 0)
                    return input + "<br />";
                else
                    return "";
            }
            if (type.Equals("Zip"))
            {
                if (input.Length > 0)
                    return input + ",";
            }
            if (type.Equals("image"))
            {
                if (input.Length > 0)
                {
                    if (!input.Contains("http://") || !input[0].Equals('/'))
                        return "<img src='/IMAGES/" + input + "'style='vertical-align: top; max-height: 95px; width: 90px;' />";
                    else
                        return "<img src='" + input + "'style='vertical-align: top; max-height: 95px; width: 90px;' />";
                }
                else
                    return "";
            }


            return input;
        }
        public String getPageID()
        {
            if (Request.QueryString["pageID"] != null)
                return Request.QueryString["pageID"].ToString();
            else
                return "";
        }
        public String checkDates(String startDate, String endDate)
        {
            DateTime start = DateTime.Parse(startDate);
            DateTime end = DateTime.Parse(endDate);

            if (start.CompareTo(end) == 0)
                return "Date: " + start.ToString("MM/dd/yy") + "<br />";
            else
                return "Dates: " + start.ToString("MM/dd/yy") + " - " + end.ToString("MM/dd/yy") + "<br />";
        }
    }
}