﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="Default.aspx.cs" MasterPageFile="~/Navigation.Master" Inherits="MobileSite.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">
  
    

    <div id="defaultgallery" class="igallery" style="z-index:-1;">
			<img class="imgBanner" src="http://1627.mobimanage.com/IMAGES/house.jpg"   alt="" style="text-align:center;width:100%;" />
			<img class="imgBanner" src="http://1627.mobimanage.com/IMAGES/house2.jpg"   alt="" style="text-align:center;width:100%;" />
			<img class="imgBanner" src="http://1627.mobimanage.com/IMAGES/house3.jpg"   alt="" style="text-align:center;width:100%;" />
		</div>
    <div style="clear:both"></div>

    <div class="foot_nav" style="text-align:center;background-color:#e9dfc7;margin-top:-43px;">
        <asp:Literal ID="menuList" runat="server"></asp:Literal>
    </div>



    <script type="text/javascript">




        //   getGallery();         

        (function ($) {
            $.fn.cycleWithResize = function (options) {

                return this.each(function () {
                    var $show = $(this);
                    initCycle(self);

                    var TO = false;
                    $(window).resize(function () {
                        if (TO !== false) {
                            clearTimeout(TO);
                        }
                        TO = setTimeout(initCycle, 50);
                    });

                    function initCycle() {
                        var cycleData = $show.data('cycle.opts'), startSlide;
                        if (cycleData) {
                            startSlide = cycleData.currSlide;
                            $show.cycle('destroy').children().removeAttr('style');
                        } else {
                            startSlide = 0;
                        }
                        var cycleOpts = $.extend(options, { startingSlide: startSlide });
                        $show.cycle(cycleOpts);
                    }
                });
            };

        })(jQuery);

        $(function () {
            $('#defaultgallery').cycleWithResize({ width: 'auto', height: 'auto' })
     
        })


    </script>



</asp:Content>






