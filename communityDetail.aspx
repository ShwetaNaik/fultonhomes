﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navigation.master" AutoEventWireup="true" CodeBehind="communityDetail.aspx.cs" Inherits="MobileSite.communityDetail" %>
<%@ Register Src="/Controls/MortgageCalculator.ascx" TagName="MC" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">
<style type="text/css">
.nav_buttons 
{
    display:block;
}
</style><form runat="server">
        <div class="detailView">
        <asp:Literal ID="communityInfoTop" runat="server"></asp:Literal>
            <asp:Label ID="mEstLabel" runat="server" Text="$0" CssClass="listingDetailContainer"></asp:Label>
             <asp:Literal ID="communityInfoBot" runat="server"></asp:Literal>
          </div>

         
            <div id="imgContainer">
                <div id="imagegallery">
                    <asp:Literal ID="communityImages" runat="server"></asp:Literal>
                </div>
            <a id="prev" href="#"><span class="P-arrow previous"></span></a><a id="next" href="#"><span class="N-arrow next"></span></a>
        </div>



  

     <%-- <div style="text-align:center;">
         <asp:Literal ID="midbar" runat="server"></asp:Literal>
     </div>--%>



            <%--- Floor Plans list ---%>
        <a href="Javascript:opPanel('#outerFpContainer', '#fpImg')" style="text-decoration:none;">
            <div class="listingMenuItem" id="fpBtn">Available Floor Plans<img alt="" id="fpImg" src="http://1627.mobimanage.com/IMAGES/triangle.png" style="float:right;margin-right:10px;padding-top:5px;" width="7" height="11" /></div>
        </a>

        <div id="outerFpContainer" class="detailDropDown" style="display: none;">
    
         <asp:Literal ID="FPList" runat="server"></asp:Literal> 
 
        </div>
 


        <%--- Move In Ready list ---%>
        <a href="Javascript:opPanel('#comm_info', '#mirImg')" style="text-decoration:none;">
            <div class="listingMenuItem" id="MIRBtn" >Call Now<img alt="" id="mirImg" src="http://1627.mobimanage.com/IMAGES/triangle.png" style="float:right;margin-right:10px;padding-top:5px;" width="7" height="11" /></div>
        </a>

        <div id="comm_info" class="detailDropDown" style="display: none;">

         <asp:Literal ID="comm_info_l" runat="server"></asp:Literal>

        </div>




        <%--- Mortgage Info ---%>
        <a href="Javascript:opPanel('#MCPanel', '#mcImg')" style="text-decoration:none;">
            <div class="listingMenuItem" id="schoolBtn" >Mortgage Calculator<img id="mcImg" src="http://1627.mobimanage.com/IMAGES/triangle.png" style="float:right;margin-right:10px;padding-top:5px;" width="7" height="11" /></div>
        </a>

        <div id="MCPanel" class="detailDropDown" style="display: none;">
            <uc1:MC ID="calc" runat="server" visible="true" />
        </div>


        <%--- Share---%>
        <a href="Javascript:opPanel('#share_panel', '#shareImg')" style="text-decoration:none;">
            <div class="listingMenuItem" id="dirBtn" >Share This Home<img id="shareImg" src="http://1627.mobimanage.com/IMAGES/triangle.png" style="float:right;margin-right:10px;padding-top:5px;" width="7" height="11" /></div>
        </a>
            <div id="share_panel" class="detailDropDown" style="display: none;">
            <asp:Literal ID="share_comm" runat="server"></asp:Literal>
           
        </div>


</form>







 <script type="text/javascript"><!--
     $(document).ready(function () {

         $('#comm_img').each(function () {
             var img_width = $(this).width();
             var img_height = $(this).height();


             var modifier = 280 / img_width;
             var new_height = img_height * modifier;

             //alert("My width:" + img_width + "px, My Height: " + img_height + "px<br />mod:" + modifier + " new height:" + new_height);

             $(this).width(280);
             $(this).height(Math.floor(new_height));

         });
      

         $('#imagegallery').cycle({
             timeout: 0,
             fx: 'scrollHorz',
             next: '#next',
             prev: '#prev',
             speed: 200,
             after: onAfter
         });

         $("#imagegallery").touchwipe({
             wipeLeft: function () {
                 $("#imagegallery").cycle("next");
             },
             wipeRight: function () {
                 $("#imagegallery").cycle("prev");
             }
         });
         
         

         function onAfter() {
             $('#captions').html(this.alt);
         }


     });

     $(window).load(function () {


     });


// --></script>




</asp:Content>
