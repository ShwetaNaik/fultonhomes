﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MobileSite
{
    public partial class Default : System.Web.UI.Page
    {
        protected static String strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["PortalConn"].ToString();
        protected static String strDBConnection = System.Configuration.ConfigurationManager.ConnectionStrings["DBConn"].ToString();
        protected String aDomainID = System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DisplayData();
            }
        }
        private void DisplayData()
        {
            //String aPageData = SqlHelper.ExecuteScalar(strConnection, CommandType.Text, "SELECT pageData FROM uMobi_Pages WHERE DomainID=" + aDomainID + " AND parentPageID=0 AND sortOrder=0").ToString();
            //litContent.Text = aPageData;

            String aSQL = "select linkName, pageType, pageRedirect, pageID, associatedImage from mobiPages WHERE domainID = " + aDomainID + "AND parentPageID = 0 AND sortOrder > 0 AND active = 1 ORDER BY SortOrder";
            DataSet aDataDS = SqlHelper.ExecuteDataset(strDBConnection, CommandType.Text, aSQL);

            DataRow drTemp;


            DataSet aPageData = SqlHelper.ExecuteDataset(strConnection, CommandType.Text, "SELECT * FROM uMobi_Domains WHERE DomainID=" + aDomainID);

            menuList.Text += string.Format("<a href=\"{1}?pageID=" +  Convert.ToInt32(aDataDS.Tables[0].Rows[5]["pageID"]) +  "\"><img src=\"http://" + aDomainID + ".mobimanage.com/IMAGES/{0}\" style=\"width:310px ;margin:5px 5px 0px 5px;\" border=\"0\" alt=\"\" /><div class=\"clear\" style=\"margin:0px;\"></div></a>", aDataDS.Tables[0].Rows[5]["associatedImage"].ToString(), aDataDS.Tables[0].Rows[5]["pageRedirect"].ToString());
            
            for (int i = 1; i < 5; i++)
            {
                drTemp = aDataDS.Tables[0].Rows[i];
                
                menuList.Text = menuList.Text + "<a href='" + checkLinkURL(drTemp["pageRedirect"].ToString(), Convert.ToInt32(drTemp["pageID"]), Convert.ToInt32(drTemp["pageType"])) +
                    "' style='text-decoration: none;'>";
                menuList.Text += string.Format("<img style=\"width:74px;margin:2px;\" src=\"http://" + aDomainID + ".mobimanage.com/IMAGES/{0}\" alt='{1}' /></a>", drTemp["associatedImage"].ToString(), drTemp["linkName"].ToString());

              //  menuList.Text += "";
            }
        }

        protected String checkLinkURL(String url, int pageID, int pageType)
        {
            if (pageType == 1)
            {
                String redir = url;
                if (redir.Contains("?")) redir += "&pageID=" + pageID;
                else redir += "?pageID=" + pageID;

                return redir;
            }
            else
            {
                return "/content.aspx?pageID=" + pageID;
            }
        }
    }
}