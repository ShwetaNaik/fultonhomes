﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.Master" CodeBehind="geoListings.aspx.cs" Inherits="MobileSite.geoListings" %>
<%@ Register Src="/Controls/geoListings.ascx" TagName="geoList" TagPrefix="uc1" %>
<%@ Register Src="/Controls/geoDetail.ascx" TagName="geoDetail" TagPrefix="uc1" %>
<%@ Register Src="/Controls/geoMap.ascx" TagName="geoMap" TagPrefix="uc1" %>

<asp:Content ID="main" ContentPlaceHolderID="cphMain" runat="server">
   <uc1:geoList ID="list" runat="server" visible="false" />
   <uc1:geoDetail ID="detail" runat="server" visible="false" />
   <uc1:geoMap ID="map" runat="server" visible="false" />
</asp:Content>
