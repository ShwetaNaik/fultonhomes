﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.Master" CodeBehind="content.aspx.cs"
    Inherits="MobileSite.content" %>

<%@ Register Src="/Controls/twitterFeed.ascx" TagName="twitterFeed" TagPrefix="uc1" %>
<%@ Register Src="/Controls/RSSFeed.ascx" TagName="Feed" TagPrefix="rss" %>
<asp:Content ID="main" ContentPlaceHolderID="cphMain" runat="server">
        <asp:Label ID="startCdiv" runat="server"></asp:Label>
            <asp:Literal runat="server" ID="pageData"></asp:Literal>
            <uc1:twitterFeed ID="twit1" runat="server" Visible="false"></uc1:twitterFeed>
            <rss:Feed ID="uclRSS" runat="server" Visible="false"></rss:Feed>
        <asp:Label ID="endCdiv" runat="server"></asp:Label>
    <asp:Literal runat="server" ID="subPages"></asp:Literal>
</asp:Content>
