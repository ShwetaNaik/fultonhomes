﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MobileSite.CUSTOM
{
    public partial class quick_move: System.Web.UI.Page
    {

        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
      //  protected String listingTable = "FULTON_Communities";
        protected int listingsPerPage = int.Parse(Utility.GetMobiSiteSettings("numListingRows"));
        Double txtlat = 0;
        Double txtlon = 0;
        String GEOUrl = "";


        protected void Page_Load(object sender, EventArgs e)
        {


            if(!IsPostBack) 
            {


              if (Request.QueryString["txtlat"] != null && Request.QueryString["txtlon"] != null)
                {

                    txtlat = Convert.ToDouble(Request.QueryString["txtlat"]);
                    txtlon = Convert.ToDouble(Request.QueryString["txtlon"]);


                    Session["txtlat"] = Request.QueryString["txtlat"];
                    Session["txtlon"] = Request.QueryString["txtlon"];

                    GEOUrl = "&txtlat=" + txtlat.ToString() + "&txtlon=" + txtlon.ToString();

                }


                if (Request.QueryString["area"] != null & Request.QueryString["state"] != null)
                {
                    areaTag.Text = "City";
                    GetCity();
                }
                else
                {

                    if (Request.QueryString["state"] != null & Request.QueryString["area"] == null)
                    {
                        areaTag.Text = "Area";
                        GetCounty();
                    }
                    else
                    { 
                        areaTag.Text = "State";
                        GetState();
                    }



                }


  
        
               // DisplayData();

            }

        }



        protected void GetCity()
        {


            var aSQL = "SELECT DISTINCT SubCity FROM BHI_Subdivision s JOIN  BHI_Builder b ON b.BuilderID = s.BuilderID WHERE SubState='" + Request.QueryString["state"] + "' AND SubCounty='"   + Request.QueryString["area"] + "' AND b.CORPORATIONID in (SELECT CORPORATIONID FROM Corporation WHERE CorporateBuilderNumber= 'FH101') ORDER BY SubCity";


            DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());



            for (int i = 0; i < aListingsDS.Tables[0].Rows.Count; i++)
            {
                DataRow dr = aListingsDS.Tables[0].Rows[i];
                areaList.Text += "<a href=\"/Listings.aspx?mir=true" + GEOUrl + "&state=" + Request.QueryString["state"] + "&town=" + dr["SubCity"].ToString() + " \"><div class=\"listingMenuItem\">" + dr["SubCity"].ToString().ToUpper() + "<img src=\"http://1627.mobimanage.com/IMAGES/searcharrow.png\" style=\"float:right;width:10px;padding-top:5px; \" /></div></a>";
     
            }

        }



        protected void GetState()
        {

    var  aSQL = "SELECT DISTINCT SubState FROM BHI_Subdivision s JOIN  BHI_Builder b ON b.BuilderID = s.BuilderID WHERE b.CORPORATIONID in (SELECT CORPORATIONID FROM Corporation WHERE CorporateBuilderNumber= 'FH101') ORDER BY SubState";


    DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());



    for (int i = 0; i < aListingsDS.Tables[0].Rows.Count; i++)

    { 
        DataRow dr = aListingsDS.Tables[0].Rows[i];
        areaList.Text += "<a href=\"/quick_move.aspx?mir=true" + GEOUrl + "&state=" + dr["SubState"] + " \"><div class=\"listingMenuItem\">" + stateExpand(dr["SubState"].ToString()).ToUpper() + "<img src=\"http://1627.mobimanage.com/IMAGES/searcharrow.png\" style=\"float:right;width:10px;padding-top:5px;\" /></div></a>";
    }

            
 }






        protected void GetCounty()
        {

            var aSQL = "SELECT DISTINCT SubCounty FROM BHI_Subdivision s JOIN  BHI_Builder b ON b.BuilderID = s.BuilderID WHERE SubState='" + Request.QueryString["state"] + "'AND b.CORPORATIONID in (SELECT CORPORATIONID FROM Corporation WHERE CorporateBuilderNumber= 'FH101') ORDER BY SubCounty";


            DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());



            for (int i = 0; i < aListingsDS.Tables[0].Rows.Count; i++)
            {
                DataRow dr = aListingsDS.Tables[0].Rows[i];
                areaList.Text += "<a href=\"/quick_move.aspx?mir=true" + GEOUrl + "&state=" + Request.QueryString["state"] + "&area=" + dr["SubCounty"] + " \"><div class=\"listingMenuItem\">" + Convert.ToString(dr["SubCounty"]).ToUpper() + "<img src=\"http://1627.mobimanage.com/IMAGES/searcharrow.png\" style=\"float:right;width:10px;padding-top:5px;\" /></div></a>";
            }


        }

        protected void DisplayData()
        {
   

               var aSQL = "SELECT DISTINCT SubState,SubCity,SubCounty FROM BHI_Subdivision s JOIN  BHI_Builder b ON b.BuilderID = s.BuilderID WHERE b.CORPORATIONID in (SELECT CORPORATIONID FROM Corporation WHERE CorporateBuilderNumber= 'FH101') ORDER BY SubState";

       DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());
            // Query for listing data
           

            // create a paging system for the data repeater
            PagedDataSource pageable = new PagedDataSource();
            pageable.DataSource = aListingsDS.Tables[0].DefaultView;


            String oldCompany = "";
   

            for (int i = 0; i < aListingsDS.Tables[0].Rows.Count; i++)
            {


                DataRow dr = aListingsDS.Tables[0].Rows[i];

                if (!dr["SubState"].ToString().Equals(oldCompany))
                {

                    areaList.Text += "<a href=\"/quick_move.aspx?mir=true" + GEOUrl + "&state=" + dr["SubState"] + " \"><div class=\"listingMenuItem\">" + dr["SubState"] + "<img src=\"http://1627.mobimanage.com/IMAGES/searcharrow.png\" style=\"float:right; display:inline-block;\" width=\"10\" /></div></a>";
   
                    oldCompany = dr["SubState"].ToString();

                }

          
            }

    //repGetlist.DataSource = pageable;
   // repGetlist.DataBind();

        }

        public string stateExpand(string abbr)
        {
            Dictionary<string, string> states = new Dictionary<string, string>();

            states.Add("AL", "Alabama");
            states.Add("AK", "Alaska");
            states.Add("AZ", "Arizona");
            states.Add("AR", "Arkansas");
            states.Add("CA", "California");
            states.Add("CO", "Colorado");
            states.Add("CT", "Connecticut");
            states.Add("DE", "Delaware");
            states.Add("DC", "District of Columbia");
            states.Add("FL", "Florida");
            states.Add("GA", "Georgia");
            states.Add("HI", "Hawaii");
            states.Add("ID", "Idaho");
            states.Add("IL", "Illinois");
            states.Add("IN", "Indiana");
            states.Add("IA", "Iowa");
            states.Add("KS", "Kansas");
            states.Add("KY", "Kentucky");
            states.Add("LA", "Louisiana");
            states.Add("ME", "Maine");
            states.Add("MD", "Maryland");
            states.Add("MA", "Massachusetts");
            states.Add("MI", "Michigan");
            states.Add("MN", "Minnesota");
            states.Add("MS", "Mississippi");
            states.Add("MO", "Missouri");
            states.Add("MT", "Montana");
            states.Add("NE", "Nebraska");
            states.Add("NV", "Nevada");
            states.Add("NH", "New Hampshire");
            states.Add("NJ", "New Jersey");
            states.Add("NM", "New Mexico");
            states.Add("NY", "New York");
            states.Add("NC", "North Carolina");
            states.Add("ND", "North Dakota");
            states.Add("OH", "Ohio");
            states.Add("OK", "Oklahoma");
            states.Add("OR", "Oregon");
            states.Add("PA", "Pennsylvania");
            states.Add("RI", "Rhode Island");
            states.Add("SC", "South Carolina");
            states.Add("SD", "South Dakota");
            states.Add("TN", "Tennessee");
            states.Add("TX", "Texas");
            states.Add("UT", "Utah");
            states.Add("VT", "Vermont");
            states.Add("VA", "Virginia");
            states.Add("WA", "Washington");
            states.Add("WV", "West Virginia");
            states.Add("WI", "Wisconsin");
            states.Add("WY", "Wyoming");
            if (states.ContainsKey(abbr))
                return (states[abbr].ToUpper());
            /* error handler is to return an empty string rather than throwing an exception */
            return "";
        }



  


    }
}