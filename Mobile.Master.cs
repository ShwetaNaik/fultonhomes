﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MobileSite
{
    public partial class Mobile : System.Web.UI.MasterPage
    {
        protected static String strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["PortalConn"].ToString();
        protected static String strDBConnection = System.Configuration.ConfigurationManager.ConnectionStrings["DBConn"].ToString();
        protected String aDomainID = System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString();
        public String aStyleSheet = Utility.GetMobiSiteSettings("StyleSheet");
        public String menuback = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DisplayData();
            }
        }
        private void DisplayData()
        {
            DataSet aPageData = SqlHelper.ExecuteDataset(strConnection, CommandType.Text, "SELECT * FROM uMobi_Domains WHERE DomainID=" + aDomainID);
      
            siteTitle.Text = aPageData.Tables[0].Rows[0]["siteTitle"].ToString();
            //imgLogo.ToolTip = aPageData.Tables[0].Rows[0]["mainLogoAltText"].ToString();
            //String aLogoWidth = Utility.GetMobiSiteSettings("mainLogoWidth");
            //if (aLogoWidth.Length > 0 && Convert.ToInt32(aLogoWidth) > 0)
                //imgLogo.Width = Convert.ToInt32(aLogoWidth);

            //imgLogo.Height = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["mainLogoHeight"].ToString());

                panMenuButton.Visible = true;
               

            if (Request.QueryString["showMenu"] != null)
            {
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = cleanQuery.Replace("&showMenu=1", "");
                cleanQuery = cleanQuery.Replace("showMenu=1", "");
                cleanQuery = cleanQuery.Replace("&showMenu=0", "");
                cleanQuery = cleanQuery.Replace("showMenu=0", "");


                if (int.Parse(Request.QueryString["showMenu"]) == 1)
                {
                    menuHyper.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + "&showMenu=0";
                    //menuback = "style='background: RGBA(103, 103, 103, .9);'";
                }
                else
                {
                    menuHyper.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&showMenu=1";
                }
            }
            else
            {
                menuHyper.NavigateUrl = Request.ServerVariables["URL"] + "?" + Request.QueryString + Request.Form + "&showMenu=1";
            }

        
        }
        
       
    }
}