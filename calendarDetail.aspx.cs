﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MobileSite
{
    public partial class calendarDetail : System.Web.UI.Page
    {
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String eventsTable = Utility.GetMobiSiteSettings("EventsTable");
        protected int listingsPerPage = Utility.GetMobiSiteSettingsInt("NumListingRows");
        int eventID = 0;
        int eventDetailLayout = 0; // set up in DB and util to enter a layout number 0 is standard
        int GEOOn = 0;
        int iconSize = 37; // set in DB to allow a new icon size vairable for listings Icons
        String emailIcon = "http://lib.mobimanage.com/ICONS/email.png"; //Add DB field and util, default given
        String webIcon = "http://lib.mobimanage.com/ICONS/web.png"; //Add DB field and util, default given
        String mapIcon = "http://lib.mobimanage.com/ICONS/map.png"; //Add DB field and util, default given
        String phoneIcon = "http://lib.mobimanage.com/ICONS/phone.png"; //Add DB field and util, default given

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["eID"] != null)
                    eventID = int.Parse(Request.QueryString["eID"]);
                if (Request.QueryString["CNM"] != null)
                {
                    Page.Title = Request.QueryString["CNM"];

                }
                DisplayData();
            }
        }

        private void DisplayData()
        {
            //Catch listingID of 0 and redirect to home page
            if (eventID == 0)
                Response.Redirect("/Default.aspx");

            // Start query
            String query = String.Format("SELECT * FROM {0} WHERE eventID = {1} ORDER BY Title", eventsTable, eventID.ToString());
            DataSet getListing = SqlHelper.ExecuteDataset(connStr, CommandType.Text, query);

            DataRow drPage = getListing.Tables[0].Rows[0];

            switch (eventDetailLayout)
            {
                case 0:
                    eventDetailInfo.Text = EventDetailLayouts.layout0(drPage, GEOOn, iconSize, emailIcon, webIcon, mapIcon, phoneIcon);
                    break;
            }


        }
    }
}