﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MobileSite
{
    public partial class Navigation : System.Web.UI.MasterPage
    {

        int sortInt = 0;
        String sortPrice = "Sort";
        String listID = "";
     

        protected void Page_Load(object sender, EventArgs e)
        {


            if (Request.UrlReferrer != null)
                {
                    if (Request.UrlReferrer.AbsolutePath != HttpContext.Current.Request.Url.AbsolutePath)
                    {
                        Session["PrevPage"] = Request.UrlReferrer.AbsoluteUri;

                    }


                    if (Request.UrlReferrer.AbsolutePath == HttpContext.Current.Request.Url.AbsolutePath && Session["PrevPage"] != null)
                    {

                        backLit.Text = "<a href=\"" + Session["PrevPage"] + "\" class=\"clsLink_Back\" style=\"color:#f3f2ef;font-family:Sans-Serif,Arial;\"> &nbsp;|&nbsp; Back</a>";
                    }
                    else
                    {
                        backLit.Text = "<a href=\"javascript:history.go(-1);\" class=\"clsLink_Back\" style=\"color:#f3f2ef;font-family:Sans-Serif,Arial;\"> &nbsp;|&nbsp; Back</a>";
                    }

                }
            if (!Page.IsPostBack)

                //if (Request.UrlReferrer.ToString() != null)
                //{
                //    backLit.Text = "<a href=\"" + Request.UrlReferrer.ToString() +"\" class=\"clsLink_Back\" style=\"color:#f3f2ef;font-family:Sans-Serif,Arial;\"> &nbsp;|&nbsp; Back</a>";
                //}
                //else
                //{
                //    
                //}

              //  Session["PrevPage"] = Request.UrlReferrer.AbsolutePath;
    
            //    if (Request.QueryString["pg"] != null)
            //    {

            //          int num = Convert.ToInt32(Request.QueryString["pg"]);
            //        try
            //        {

            //             num = num / -20;
            //        }
            //        catch (Exception ex) { num = -1;  }
               

            //        backLit.Text = "<a href=\"javascript:history.go(" + num + ");\" class=\"clsLink_Back\" style=\"color:#f3f2ef;font-family:Sans-Serif,Arial;\"> &nbsp;|&nbsp; Back</a>";

            //    }

            //    else
            //    {


            //backLit.Text ="<a href=\"javascript:history.go(-1);\" class=\"clsLink_Back\" style=\"color:#f3f2ef;font-family:Sans-Serif,Arial;\"> &nbsp;|&nbsp; Back</a>";

            //    }

            if (Request.QueryString["listID"] != null)
                listID = Request.QueryString["listID"];


                       bool favChecked = false;


            if (Request.Cookies["community"] != null && Request.Cookies["community"].Value.Contains(listID.ToString()))
                favChecked = true;


            if (this.Parent.Page.AppRelativeVirtualPath.ToLower().Contains("listingdetail.aspx"))
            {

                        if (favChecked)
                            favbar.Text = "<a href=\"Javascript:cycleFav('" + listID + "', 'MIRHouse')\" style='text-decoration:none;'><div class=\"nav_buttons\" id=\"favBtn\" style=\"background-color: #FFF; color: #660000;\">Saved</div></a>";
                        else
                            favbar.Text = "<a href=\"Javascript:cycleFav('" + listID + "', 'MIRHouse')\" style='text-decoration:none;'><div class=\"nav_buttons\" id=\"favBtn\">Save</div></a>";

            }


            if (this.Parent.Page.AppRelativeVirtualPath.ToLower().Contains("communitydetail.aspx"))
            {
                        if (favChecked)
                            favbar.Text = "<a href=\"Javascript:cycleFav('" + listID + "', 'community')\" style='text-decoration:none;'><div class=\"nav_buttons\" id=\"favBtn\" style=\"background-color: #FFF; color: #660000;\">Saved</div></a>";
                        else
                            favbar.Text = "<a href=\"Javascript:cycleFav('" + listID + "', 'community')\" style='text-decoration:none;'><div class=\"nav_buttons\" id=\"favBtn\">Save</div></a>";

            }





                if (this.Parent.Page.AppRelativeVirtualPath.ToLower().Contains("default.aspx"))

                    NavBarPanel.Visible = false;
                else
                    NavBarPanel.Visible = true;

            if (this.Parent.Page.AppRelativeVirtualPath.ToLower().Contains("listings.aspx") || this.Parent.Page.AppRelativeVirtualPath.ToLower().Contains("CommunityListings.aspx"))
            {



                if (Convert.ToInt32(Request.QueryString["sort"]) == 0)
                {
                    sortInt = 1;
                    sortPrice = "High";

                }
                else
                {
                    sortPrice = "Low";
                }

                this.sortBtn.Text += "<a class=\"nav_buttons\" style=\"text-decoration:none;color:#660000;\" href=\"" + Request.Url.PathAndQuery.Replace("&sort=0", "").Replace("&sort=1", "") + "&sort=" + sortInt + "\">Price " + sortPrice + "</a>";
                
              


                DisplayData();

  sortInt = 0;

            }

        }


        private void DisplayData()
        {
            int pageID = 0;
            String CNM = "";
            String listID = "";
            String lat = "0";
            String lon = "0";
     
            Boolean Mapable = false;
            String mapType = "map";
            String comMIR = "MIR";
            String strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["DBConn"].ToString();
            String aDomainID = System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString();
            if (Request.QueryString["pageID"] != null)
            {
                pageID = int.Parse(Request.QueryString["pageID"]);
                Session["PageID"] = pageID;
            }

            if (Request.QueryString["CNM"] != null)
            {
                CNM = Request.QueryString["CNM"];  
            }

            if (Request.QueryString["listID"] != null)
            {
                listID = Request.QueryString["listID"];
            }
            if (Request.QueryString["txtlat"] != null)
            {
                lat = Request.QueryString["txtlat"];
            }

            if (Request.QueryString["txtlon"] != null)
            {
                lon = Request.QueryString["txtlon"];
            }

            String reUrl = this.Parent.Page.AppRelativeVirtualPath.ToLower();


        
                 



            if (reUrl.Contains("/listings.aspx".ToLower()))
            {
                Mapable = true;
                comMIR = "COM";
                lblCurrentPage.Visible =false;

                
            } else if (reUrl.Contains("/communityDetail.aspx".ToLower()))
            {
                Mapable = false;
                mapType = "detail";
                comMIR = "COM";
                
            } else if (reUrl.Contains("/communityListings.aspx".ToLower()))
            {
             
                Mapable = true;
                comMIR = "COM";
                
            } else if (reUrl.Contains("/listingDetails.aspx".ToLower()))
            {
               Mapable = false;
               mapType = "detail";

            }
            else if (reUrl.Contains("/geolistings.aspx".ToLower()))
            {
                sortBtn.Visible = false;
                navbar.Text += "<a href=\"javascript:history.go(-1);\"><div class=\"nav_buttons\" style=\"float: right; margin-right:8px;text-decoration: none;\">List</div></a>";
            }

   // Response.Write("<script>alert('" + Request.Url.AbsolutePath.ToString().ToLower() + "');</script>");

        //   if (Request.QueryString["txtlat"] != null && Request.QueryString["txtlon"] != null && showMap ==true)
          //  {
                
          //  }
         
                  //Favorite BETA
            //FAVORITES
 
            // Handlie the nav buttons in the back bar (topNav)
            //TODO
            // add if for plans, MIR, comm homes
            if (Mapable == true)
            {
                String geoURL = "/geoListings.aspx?redirTo=" + mapType + "&type=" + comMIR;
                if (!lat.Equals("0"))
                {
                    geoURL += "&txtLat=" + lat + "&txtLon=" + lon;
                }
                else //No lat/lon, grab city, state
                {
                    if (mapType != "detail")
                    {
                        String city = "";
                        String state = "";
                        if (Request.QueryString["town"] != null)
                            city = Request.QueryString["town"].ToString();
                        if (Request.QueryString["city"] != null)
                            city = Request.QueryString["city"].ToString();
                        if (Request.QueryString["state"] != null)
                            state = Request.QueryString["state"].ToString();

                        geoURL += "&state=" + state + "&town=" + city;
                    }
                    else
                    {
                        if (Request.QueryString["listID"] != null)
                            geoURL += "&listID=" + Request.QueryString["listID"].ToString();
                    }
                }

                navbar.Text += "<a href=\"" + geoURL + "\" id=\"mapLink\"><div class=\"nav_buttons\" style=\"float: right; margin-right:8px;text-decoration: none;\">Map</div></a>";

            }



           





            DataSet dsPage = SqlHelper.ExecuteDataset(strConnection, CommandType.Text, String.Format("SELECT pageData, linkName, rsslink, rssType, sortOrder, pageType, pageRedirect FROM mobiPages WHERE domainID = {0} AND pageID = {1}", aDomainID, pageID));
            int aSubNavCount = Convert.ToInt32(SqlHelper.ExecuteScalar(strConnection, CommandType.Text, String.Format("SELECT COUNT(*) from mobiPages WHERE DomainID={0} AND ParentpageID={1} and active=1", aDomainID, pageID)));
            if (Utility.GetMobiSiteSettingsInt("ShowNavigation") == 1)// && aSubNavCount == 0)
            {
                botNav.Visible = true;
                topNav.Visible = true;
                if (pageID > 0)
                {
                   lblCurrentPage.Text = dsPage.Tables[0].Rows[0]["linkName"].ToString();
                   // lblCurrentPage2.Text = dsPage.Tables[0].Rows[0]["linkName"].ToString();
                }
                else if (Session["CurrentPageName"] != null)
                {
                    lblCurrentPage.Text = Session["CurrentPageName"].ToString();
                    //lblCurrentPage2.Text = Session["CurrentPageName"].ToString();
                }
                else if (CNM.Length > 0)
                {
                   // lblCurrentPage.Text = CNM;
                }
            }
            else
            {


                botNav.Visible = false;
                topNav.Visible = false;
            }
        }

        protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            e.Item.DataItem= "help";
        }
    }
}