﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.Master" CodeBehind="Weather.aspx.cs" Inherits="MobileSite.Weather" %>
<%@ Register Src="/Controls/weather.ascx" TagName="weatherInclude" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <div class="contentOuter">
        <div class="content">
            <uc1:weatherInclude ID="weather1" runat="server" />
        </div>
    </div>
</asp:Content>
