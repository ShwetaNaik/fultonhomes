﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MobileSite
{
    public partial class Search : System.Web.UI.Page
    {
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String listingTable = Utility.GetMobiSiteSettings("ListingsTable");
        protected String accountID = "10048"; //System.Configuration.ConfigurationManager.AppSettings["accountID"].ToString();
        protected int listingsPerPage = int.Parse(Utility.GetMobiSiteSettings("numListingRows"));
        int sRow = 0;
        String listSrch = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Page.Title = "Search";
                if (Request.QueryString["listSrch"] != null)
                {
                    listSrch = Request.QueryString["listSrch"].ToString();
                }
                if (Request.QueryString["sRow"] != null)
                    sRow = int.Parse(Request.QueryString["sRow"]);
                DisplayData();
            }
        }

        private void DisplayData()
        {
            // Query for listing data
            String query = "SELECT DISTINCT listingID, Company, address1, city, state, zip, image_list FROM " + listingTable + " WHERE ACCOUNTID = " + accountID;
            if (listSrch.Length > 0)
                query = query + " AND (Company like '%" + listSrch.Replace(' ', '%') + "%' OR Description  like '%" + listSrch.Replace(' ', '%') +
                    "%'or categoryName  like '%" + listSrch.Replace(' ', '%') + "%')";
            query = query + " ORDER BY Company";
            DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, query);

            // create a paging system for the data repeater
            PagedDataSource pageable = new PagedDataSource();
            pageable.DataSource = aListingsDS.Tables[0].DefaultView;

            //turn on paging
            pageable.AllowPaging = true;
            //set the start row to a sRow (can be passed in URL)
            pageable.CurrentPageIndex = sRow;
            //set listings per page (set in config)
            pageable.PageSize = listingsPerPage;

            repGetlist.DataSource = pageable;
            repGetlist.DataBind();

            // Set Page number display
            int currPage = sRow + 1;
            pageTotal.Text = "PAGE " + currPage + " OF " + pageable.PageCount;

            //Previous and Next links
            div1.Text = " | ";
            div2.Text = " | ";
            if (sRow == 0)
            {
                prevBtn.Visible = false;
                prevBtn2.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
            }
            else
            {
                int prevRow = sRow - 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "&sRow=[0-9]", "", RegexOptions.IgnoreCase);
                prevBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
                prevBtn2.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
            }
            if (pageable.IsLastPage)
            {
                nextBtn.Visible = false;
                nextBtn2.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
            }
            else
            {
                int nextRow = sRow + 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "&sRow=[0-9]", "", RegexOptions.IgnoreCase);
                nextBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
                nextBtn2.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
            }

        }

        public String checkLen(String input, String type)
        {
            if (type.Equals("Address"))
            {
                if (input.Length > 0)
                    return input + "<br />";
                else
                    return "";
            }
            if (type.Equals("Zip"))
            {
                if (input.Length > 0)
                    return input + ",";
            }
            if (type.Equals("image"))
            {
                if (input.Length > 0)
                {
                    if (!input.Contains("http://"))
                        return "<img src='/IMAGES/" + input + "'style='vertical-align: top; max-height: 95px; width: 90px;' />";
                    else
                        return "<img src='" + input + "'style='vertical-align: top; max-height: 95px; width: 90px;' />";
                }
                else
                    return "";
            }


            return input;
        }

    }
}