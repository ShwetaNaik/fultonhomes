﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MobileSite
{
    public partial class geoCat : System.Web.UI.Page
    {
        protected double defLat = Utility.GetMobiSiteSettingsDbl("DefaultLatitude");
        protected double DefLon = Utility.GetMobiSiteSettingsDbl("DefaultLongitude");

        protected void Page_Load(object sender, EventArgs e)
        {
            txtxlatLine1.Text = "<input type='hidden' id='txtLat1' name='txtLat' value='" + defLat.ToString() + "' />";
            txtLongLine1.Text = "<input type='hidden' id='txtLong1' name='txtLong' value='" + DefLon.ToString() + "' />";

            txtxlatLine2.Text = "<input type='hidden' id='txtLat2' name='txtLat' value='" + defLat.ToString() + "' />";
            txtLongLine2.Text = "<input type='hidden' id='txtLong2' name='txtLong' value='" + DefLon.ToString() + "' />";

            txtxlatLine3.Text = "<input type='hidden' id='txtLat3' name='txtLat' value='" + defLat.ToString() + "' />";
            txtLongLine3.Text = "<input type='hidden' id='txtLong3' name='txtLong' value='" + DefLon.ToString() + "' />";

            txtxlatLine4.Text = "<input type='hidden' id='txtLat4' name='txtLat' value='" + defLat.ToString() + "' />";
            txtLongLine4.Text = "<input type='hidden' id='txtLong4' name='txtLong' value='" + DefLon.ToString() + "' />";
        }
    }
}