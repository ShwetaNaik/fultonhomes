﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace MobileSite
{
    public partial class Deals : System.Web.UI.Page
    {
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String dealsTable = Utility.GetMobiSiteSettings("OffersTable");
        protected int listingsPerPage = Utility.GetMobiSiteSettingsInt("NumListingRows");
        int sRow = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["sRow"] != null)
                    sRow = int.Parse(Request.QueryString["sRow"]);

                DisplayData();
            }
        }
        private void DisplayData()
        {
            DateTime today = DateTime.Now;

            String query = String.Format("SELECT DISTINCT offerID, Title, Company, RedeemStart, RedeemEnd FROM {0} WHERE REDEEMEND >= '{1}' ORDER BY Title", dealsTable, today.ToString("MM/dd/yyyy"));
            DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, query);

            // create a paging system for the data repeater
            PagedDataSource pageable = new PagedDataSource();
            pageable.DataSource = aListingsDS.Tables[0].DefaultView;

            //turn on paging
            pageable.AllowPaging = true;
            //set the start row to a sRow (can be passed in URL)
            pageable.CurrentPageIndex = sRow;
            //set listings per page (set in config)
            pageable.PageSize = listingsPerPage;

            repGetlist.DataSource = pageable;
            repGetlist.DataBind();

            // Set Page number display
            int currPage = sRow + 1;
            pageTotal.Text = "PAGE " + currPage + " OF " + pageable.PageCount;

            //Previous and Next links
            div1.Text = " | ";
            div2.Text = " | ";
            if (sRow == 0)
            {
                prevBtn.Visible = false;
                prevBtn2.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
            }
            else
            {
                int prevRow = sRow - 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "&sRow=[0-9]", "", RegexOptions.IgnoreCase);
                cleanQuery = Regex.Replace(cleanQuery, "sRow=[0-9]", "", RegexOptions.IgnoreCase);
                prevBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
                prevBtn2.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
            }
            if (pageable.IsLastPage)
            {
                nextBtn.Visible = false;
                nextBtn2.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
            }
            else
            {
                int nextRow = sRow + 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "&sRow=[0-9]", "", RegexOptions.IgnoreCase);
                cleanQuery = Regex.Replace(cleanQuery, "sRow=[0-9]", "", RegexOptions.IgnoreCase);
                nextBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
                nextBtn2.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
            }
        }

        public String printDate(String start, String end)
        {
            String output = "Valid: ";

            DateTime sDate = DateTime.Parse(start);
            DateTime eDate = DateTime.Parse(end);

            output += sDate.ToString("MM/dd/yy") + " - " + eDate.ToString("MM/dd/yy");

            return output;
        }
    }
}