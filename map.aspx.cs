﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MobileSite
{
    public partial class map : System.Web.UI.Page
    {
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String listingTable = Utility.GetMobiSiteSettings("ListingsTable");
        protected String accountID = "1496"; //System.Configuration.ConfigurationManager.AppSettings["accountID"].ToString();

        protected String CID = "0";
        protected String listID = "0";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["CID"] != null)
                {
                    CID = Request.QueryString["CID"].ToString();
                }
                if (Request.QueryString["listID"] != null)
                {
                    listID = Request.QueryString["listID"].ToString();
                }

                DisplayData();
            }
        }

        private void DisplayData()
        {

            if (listID != "0")
            {
                String Query = "SELECT DISTINCT TOP(1) listingID, Company, address1, city, state, zip, image_list, phone, latitude, longitude" +
                " FROM " + listingTable + " WHERE ACCOUNTID = " +
                accountID + " AND listingID = "
                + listID;

                DataSet byTypeDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, Query);

                DataRow drTemp = byTypeDS.Tables[0].Rows[0];

                if (drTemp != null)
                {
                    display.Text += "<h2>" + drTemp["Company"].ToString() + "</h2>" + drTemp["Address1"].ToString() + " " + drTemp["City"].ToString() + ", " +
                        drTemp["State"].ToString() + " " + drTemp["zip"].ToString() + "<br /><br />";

                    //if lat & long use them
                    if ( drTemp["latitude"].ToString().Length > 2 && drTemp["longitude"].ToString().Length > 2){

                        display.Text += "<img style='margin-left: -12px;' src='http://maps.google.com/maps/api/staticmap?center=" + drTemp["latitude"].ToString() + "," + drTemp["longitude"].ToString() +
                            "&size=280x350&zoom=13&markers=color:blue%7C#latitude#,#longitude#&sensor=true' />";
                    } else{

                        String cleanAddr = drTemp["Address1"].ToString().Replace(" ", "+");
                        String cleanCity = drTemp["City"].ToString().Replace(" ", "+");
                        String cleanState = drTemp["state"].ToString().Replace(" ", "+");

                        display.Text += "<img style='margin-left: -12px;' src='http://maps.google.com/maps/api/staticmap?center=" + cleanAddr + "," + cleanCity + "," + cleanState 
                            + "&size=280x350&zoom=13&sensor=true&markers=color:blue%7C" + cleanAddr + "," + cleanCity + "," + cleanState + "' />";
                    }



                        


                }
                else
                {
                    display.Text = "Sorry, no map data available at this time";
                }

            }
        }
    }
}