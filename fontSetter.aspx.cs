﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MobileSite
{
    public partial class fontSetter : System.Web.UI.Page
    {
        protected String size;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["size"] != null)
                    size = Request.QueryString["size"].ToString();

                DisplayData();
            }
        }

        protected void DisplayData()
        {
            if (size.Length > 0)
            {
                if (size == "smaller" || size == "larger")
                {
                    if (Request.Cookies["fontSize"] == null)
                        Response.Cookies["fontSize"].Value = "M";

                    if (Request.Cookies["fontSize"] != null)
                    {
                        if (Request.Cookies["fontSize"].Value == "M")
                        {
                            if (size == "smaller")
                                Response.Cookies["fontSize"].Value = "S";
                            else
                                Response.Cookies["fontSize"].Value = "L";

                        }
                        else if (Request.Cookies["fontSize"].Value == "L" && size == "smaller")
                        {
                            Response.Cookies["fontSize"].Value = "M";
                        }
                        else if (Request.Cookies["fontSize"].Value == "S" && size == "larger")
                        {
                            Response.Cookies["fontSize"].Value = "M";
                        }
                    }
                }
                else
                    Response.Cookies["fontSize"].Value = size;
            }

            Response.Redirect(Request.UrlReferrer.ToString());
        }
    }
}