﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.master" CodeBehind="advanced_search.aspx.cs" Inherits="MobileSite.advanced_search" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">

<form method="post" runat="server">

<div class="title_bar">Available Homes Search</div>
<div class=clear"></div>


<div class="divForm" style="background-color:#FFF;padding:10px;">

<h2>City (AZ):</h2>

<asp:DropDownList ID="town" runat="server" 
              DataTextField="SubCity" DataValueField="SubCity">
        </asp:DropDownList>

<h2>Price Range:</h2>
        <asp:DropDownList ID="pmin" runat="server">
            <asp:ListItem Value="0">No Min</asp:ListItem>
            <asp:ListItem Value="100000">$100,000</asp:ListItem>
            <asp:ListItem Value="125000">$125,000</asp:ListItem>
            <asp:ListItem Value="15000">$150,000</asp:ListItem>
            <asp:ListItem Value="175000">$175,000</asp:ListItem>
            <asp:ListItem Value="200000">$200,000</asp:ListItem>
            <asp:ListItem Value="250000">$250,000</asp:ListItem>
            <asp:ListItem Value="300000">$300,000</asp:ListItem>
            <asp:ListItem Value="350000">$350,000</asp:ListItem>
            <asp:ListItem Value="400000">$400,000</asp:ListItem>
            <asp:ListItem Value="400000">$500,000</asp:ListItem>
            <asp:ListItem Value="750000">$750,000</asp:ListItem>
            <asp:ListItem Value="1000000">$1,000,000</asp:ListItem>
            <asp:ListItem Value="1250000">$1,250,000</asp:ListItem>



        </asp:DropDownList>
        -
        <asp:DropDownList ID="pmax" runat="server">
            <asp:ListItem Value="0">No Max</asp:ListItem>
            <asp:ListItem Value="100000">$100,000</asp:ListItem>
            <asp:ListItem Value="125000">$125,000</asp:ListItem>
            <asp:ListItem Value="15000">$150,000</asp:ListItem>
            <asp:ListItem Value="175000">$175,000</asp:ListItem>
            <asp:ListItem Value="200000">$200,000</asp:ListItem>
            <asp:ListItem Value="250000">$250,000</asp:ListItem>
            <asp:ListItem Value="300000">$300,000</asp:ListItem>
            <asp:ListItem Value="350000">$350,000</asp:ListItem>
            <asp:ListItem Value="400000">$400,000</asp:ListItem>
            <asp:ListItem Value="500000">$500,000</asp:ListItem>
            <asp:ListItem Value="750000">$750,000</asp:ListItem>
            <asp:ListItem Value="1000000">$1,000,000</asp:ListItem>
            <asp:ListItem Value="1500000">$1,500,000</asp:ListItem>
            <asp:ListItem Value="2000000">$2,000,000</asp:ListItem>
            <asp:ListItem Value="2500000">$2,500,000</asp:ListItem>
        </asp:DropDownList>



         <h2>Square Footage:</h2>
        <asp:DropDownList ID="sqft" runat="server">
            <asp:ListItem>0</asp:ListItem>
            <asp:ListItem>500</asp:ListItem>
            <asp:ListItem>1000</asp:ListItem>
            <asp:ListItem>1500</asp:ListItem>
            <asp:ListItem>2000</asp:ListItem>
            <asp:ListItem>2500</asp:ListItem>
            <asp:ListItem>3000</asp:ListItem>
            <asp:ListItem>4000</asp:ListItem>
            <asp:ListItem>5000</asp:ListItem>
        </asp:DropDownList>
        -
           <asp:DropDownList ID="maxsqft" runat="server">
            <asp:ListItem>No Max</asp:ListItem>
          <asp:ListItem>500</asp:ListItem>
            <asp:ListItem>1000</asp:ListItem>
            <asp:ListItem>1500</asp:ListItem>
            <asp:ListItem>2000</asp:ListItem>
            <asp:ListItem>2500</asp:ListItem>
            <asp:ListItem>3000</asp:ListItem>
            <asp:ListItem>4000</asp:ListItem>
            <asp:ListItem>5000</asp:ListItem>
        </asp:DropDownList>


       <h2>Stories:</h2>
                <asp:DropDownList ID="stories" runat="server" 
                DataTextField="Story" DataValueField="Story">
            <asp:ListItem>All</asp:ListItem>
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
            <asp:ListItem>7</asp:ListItem>
            <asp:ListItem>8</asp:ListItem>
            <asp:ListItem>9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
        </asp:DropDownList>


       <h2>Bedrooms:</h2>
        <asp:DropDownList ID="beds" runat="server">
            <asp:ListItem>0</asp:ListItem>
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
        </asp:DropDownList>
        -
           <asp:DropDownList ID="maxbeds" runat="server">
            <asp:ListItem>No Max</asp:ListItem>
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
        </asp:DropDownList>

<h2>Full Baths:</h2>
         <asp:DropDownList ID="baths" runat="server" ClientIDMode="Static">
                    <asp:ListItem>0</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
        </asp:DropDownList>
        -
           <asp:DropDownList ID="maxbaths" runat="server">
           <asp:ListItem>No Max</asp:ListItem>
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
        </asp:DropDownList>


        <h2>Half Baths:</h2>
         <asp:DropDownList ID="halfbaths" runat="server" ClientIDMode="Static">
                    <asp:ListItem>0</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
        </asp:DropDownList>
        -
           <asp:DropDownList ID="maxhalfbaths" runat="server">
           <asp:ListItem>No Max</asp:ListItem>
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
        </asp:DropDownList>

        <br /><br />

<asp:Button ID="results" runat="server" Text="Search" onClick="results_Click" style="padding:5px;font-weight:bold;font=-family:helvetica; font-size;13pt; width:150px;" />



    </div>

    </form>

</asp:Content>