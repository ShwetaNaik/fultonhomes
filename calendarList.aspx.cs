﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace MobileSite
{
    public partial class calendarList : System.Web.UI.Page
    {
        int sRow = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["sRow"] != null)
                    sRow = int.Parse(Request.QueryString["sRow"]);

                DisplayData();
            }
        }
        private void DisplayData()
        {
            // Get the current month and year
            DateTime today = DateTime.Now;
            DateTime actualToday = DateTime.Now;

            // if there's an offset add it in
            today = today.AddMonths(sRow);

            int month = today.Month;
            int year = today.Year;
            int day = today.Day;

            // get the first day of the month
            DateTime first = new DateTime(year, month, 1);

           CalTitle.InnerText = today.ToString("MMMM yyyy");

            // handle month changing
            if (sRow == 0)
            {
                prevBtn.Visible = false;
            }
            else
            {
                int prevRow = sRow - 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "&sRow=[0-9]", "", RegexOptions.IgnoreCase);
                cleanQuery = Regex.Replace(cleanQuery, "sRow=[0-9]", "", RegexOptions.IgnoreCase);
                prevBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
            }
            //Cap future at two years
            if (sRow == 23)
            {
                nextBtn.Visible = false;
            }
            else
            {
                int nextRow = sRow + 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "&sRow=[0-9]", "", RegexOptions.IgnoreCase);
                cleanQuery = Regex.Replace(cleanQuery, "sRow=[0-9]", "", RegexOptions.IgnoreCase);
                nextBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
            }

            // Print Days of week
            for (int i = 0; i < 7; i++)
            {
                DayOfWeek loop = (DayOfWeek)i;
                table1.Rows[1].Cells[i].Text = loop.ToString();
                table1.Rows[2].Cells[i].Text = loop.ToString().Substring(0, 3);
            }

            int dayCount = 1;

            // This starts at row 3 accounting 1 row for month name, 1 for full days, 1 for abbreviated names
            for( int i = (int)first.DayOfWeek; i < (int)first.DayOfWeek + System.DateTime.DaysInMonth(year, month); i ++){

                
                HyperLink hype = new HyperLink();
                if (month == actualToday.Month && year == actualToday.Year && dayCount < actualToday.Day)
                {
                    hype.Text = "<div class='calBlockInactive'>" + dayCount.ToString() + "</div>";
                }
                else
                {
                    hype.NavigateUrl = "/calendarDateList.aspx?month=" + month + "&year=" + year + "&day=" + dayCount;
                    hype.Text = "<div class='calBlock'>" + dayCount.ToString() + "</div>";
                }
                
                table1.Rows[(i / 7) + 3].Cells[i % 7].Controls.Add(hype);
                dayCount++;

            }

            
        }
    }
}