﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MobileSite
{
    public partial class listings : System.Web.UI.Page
    {
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String listingTable = "BHI_Specs";
        protected int listingsPerPage = 20;
        protected Boolean GEOON = Utility.GetCustomerSettingsBoolean("bGEO");
        int CID = 0;
        int sRow = 0;

        int pageCount = 20;


        String town = "";
        String stories = "";

        String priceMin = "";
        String priceMax = "";
        String sqft = "";
        String maxSqft = "";
        String beds = "";
        String maxBeds = "";
        String baths = "";

        Boolean mir = false;
        Double txtlon = 0;
        Double txtlat = 0;

        String maxhalfBaths = "";
        String halfbaths = "";
        String maxBaths = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["showMap"] = true;
                //if (Request.QueryString["CID"] != null)
                // CID = Request.QueryString["CID"];

                if (Request.QueryString["sRow"] != null)
                    sRow = int.Parse(Request.QueryString["sRow"]);

                if (Request.QueryString["CID"] != null)
                    CID = int.Parse(Request.QueryString["CID"]);

                if (Request.QueryString["txtlat"] != null)
                    txtlat = Convert.ToDouble(Request.QueryString["txtlat"]);
                if (Request.QueryString["txtlon"] != null)
                    txtlon = Convert.ToDouble(Request.QueryString["txtlon"]);


                if (Request.QueryString["pg"] != null)
                    pageCount = Convert.ToInt32(Request.QueryString["pg"].ToString());
                
                // search criteria

                if (Request.QueryString["town"] != null)
      
                    town = Request.QueryString["town"];




  


                if (Request.QueryString["priceMin"] != null)
                    priceMin = Request.QueryString["priceMin"];

                if (Request.QueryString["priceMax"] != null)
                    priceMax = Request.QueryString["priceMax"];

                if (Request.QueryString["baths"] != null)
                    baths = Request.QueryString["baths"];

                if (Request.QueryString["maxbaths"] != null)
                    maxBaths = Request.QueryString["maxBaths"];

                if (Request.QueryString["halfbaths"] != null)
                    halfbaths = Request.QueryString["halfbaths"];

                if (Request.QueryString["maxhalfbath"] != null)
                    maxhalfBaths = Request.QueryString["maxhalfBaths"];


                if (Request.QueryString["maxsqft"] != null)
                    maxSqft = Request.QueryString["maxsqft"];

                if (Request.QueryString["sqft"] != null)
                    sqft = Request.QueryString["sqft"];

                if (Request.QueryString["beds"] != null)
                    beds = Request.QueryString["beds"];
                if (Request.QueryString["maxbeds"] != null)
                    maxBeds = Request.QueryString["maxbeds"];



                if (Request.QueryString["MIR"] != null)
                    mir = Convert.ToBoolean(Request.QueryString["mir"]);

                if (Request.QueryString["stories"] != null)
                    stories = Request.QueryString["stories"];

                if (Request.QueryString["search"] != null)
                {
                    if (Request.QueryString["search"] == "plans")
                    {
                        this.TitleTag.Text = "Available Floorplans";
                        DisplayPlans();
                    }
                    else
                    {
                        this.TitleTag.Text = "Available Quick Move-In Homes";
                        DisplayQDH(); 
                    }
                   

                }

                else
                {
                    this.TitleTag.Text = "Available Quick Move-In Homes";
                    DisplayQDH();
                }


             }
        }

 
       

#region Listings "Show Specs"

        private void DisplayQDH()
        {

            if (listingsPerPage == 0)
                listingsPerPage = 200;


            //CHECK FOR MOVEIN READY CHECK
            String aSQL = "";

 
                // Go check the database  FOR SPECS
                aSQL = String.Format("SELECT Distinct(e.SpecStreet1),s.SubDivisionName,e.SpecNumber,e.SpecZip,e.SpecCity,e.SpecState,e.SpecCounty, e.SpecSqFt,e.SpecBaths,e.SpecBedrooms, e.SpecID,e.SpecMoveInDate,e.SpecGarage,e.SpecPrice, s.SubDivisionID, e.PlanID,a.PlanName FROM BHI_Plans a Inner Join BHI_Specs e on a.planID=e.planID JOIN BHI_SubDivision s "
     + "on s.subDivisionID=a.SubdivisionID JOIN BHI_Builder b ON b.BuilderID=s.BuilderID "
                      + " WHERE B.BUILDERID IN (SELECT DISTINCT BuilderID FROM Builder"
                      + " WHERE CORPORATIONID IN (SELECT DISTINCT CORPORATIONID "
                      + " FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101')) ", listingTable);

     
                if (CID > 0)
                    aSQL += " AND E.PlanID =" + CID + " ";
          
                if (stories.Length > 0)
                    aSQL += " AND E.SpecStories = " + stories.Replace("'", "") + " ";

                if (town.Length > 0)
                    aSQL += " AND E.SpecCity = '" + town.Replace("'", "") + "'";
              
                if (priceMin.Length > 0)
                    aSQL += " AND e.SpecPrice >= " + priceMin.Replace("'", "") + " ";
                if (priceMax.Length > 0)
                    aSQL += " AND e.SpecPrice <= " + priceMax.Replace("'", "") + " ";

                if (sqft.Length > 0)
                    aSQL += " AND e.SpecSqFt >= " + sqft.Replace("'", "") + " ";
                if (maxSqft.Length > 0)
                    aSQL += " AND e.SpecSqFt <= " + maxSqft.Replace("'", "") + " ";

                if (beds.Length > 0)
                    aSQL += " AND e.SpecBedrooms >= " + beds.Replace("'", "") + " ";
                if (maxBeds.Length > 0)
                    aSQL += " AND e.SpecBedrooms <= " + maxBeds.Replace("'", "") + " ";

                if (baths.Length > 0)
                    aSQL += " AND e.SpecBaths >= " + baths.Replace("'", "") + " ";
                if (maxBaths.Length > 0)
                    aSQL += " AND e.SpecBaths <= " + maxBaths.Replace("'", "") + " ";

                if (halfbaths.Length > 0)
                    aSQL += " AND e.SpecHalfBaths >= " + halfbaths.Replace("'", "") + " ";
                if (maxhalfBaths.Length > 0)
                    aSQL += " AND e.SpecHalfBaths <= " + maxhalfBaths.Replace("'", "") + " ";

                //    aSQL += " ORDER BY e.SpecPrice, e.SpecStreet1  ";
        
            
                       if (Request.QueryString["sort"] != null)
                {
                    if (Convert.ToInt32(Request.QueryString["sort"]) == 0)
                        aSQL += " ORDER BY e.SpecPrice ASC ";
                    else
                        aSQL += " ORDER BY e.SpecPrice DESC ";

                }

      







            DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());

            int startRow = sRow * listingsPerPage;
            int endRow = pageCount;

            if (endRow > aListingsDS.Tables[0].Rows.Count)
                endRow = aListingsDS.Tables[0].Rows.Count;


            String smir = "";
            String sort = "";
            String stown = "";
            if (pageCount <= aListingsDS.Tables[0].Rows.Count)
            {
                if (Request.QueryString["sort"] != null)
                    sort = "&sort=" + Request.QueryString["sort"].ToString();
                if (Request.QueryString["mir"] != null)
                    smir = "&mir=" + Request.QueryString["mir"].ToString();
                if (Request.QueryString["town"] != null)
                    stown = "&town=" + Request.QueryString["town"].ToString();

                this.commBtn.Text = "<a name=\"target\" href=\"Listings.aspx?&pg=" + (pageCount + listingsPerPage) + sort + smir + stown + "\" style=\"text-decoration:none;\"/><div id=\"comm_btn\" class=\"loadBtn\">Load More Homes</div></a>";
            }
            else
            {
                this.commBtn.Text = "<div id=\"comm_btn\" class=\"loadBtn\" style=\"background-color:#f8f8f8;cursor:default;color:#666;display:none;\">Completed</div>";
            }


            for (int i = 0; i < endRow; i++)
            {
                DataRow dr = aListingsDS.Tables[0].Rows[i];



                // Check for an image


                String imageQuery = "SELECT DISTINCT d.Value FROM BHI_SubDivision s JOIN BHI_Builder b ON b.BuilderID=s.BuilderID JOIN BHI_Plans c ON s.SubDivisionID=c.SubDivisionID " +
                                     "JOIN BHI_Specs h on h.PlanID=c.PlanID LEFT JOIN BHI_SpecImages d ON d.SpecID=h.SpecID WHERE B.BUILDERID IN (SELECT DISTINCT BuilderID " +
                                     "FROM Builder WHERE d.SpecID ='" + dr["SpecID"] + "' AND CORPORATIONID IN (SELECT DISTINCT CORPORATIONID FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101'))";



                //  String imageQuery = "SELECT CLOUDPATH FROM OLTH_IMAGES WHERE ISPRIMARY = 1 AND LISTINGID = " + dr["specID"];
                DataSet aImageSet = SqlHelper.ExecuteDataset(connStr, CommandType.Text, imageQuery);




                homeList.Text += "<a href=\"/listingDetail.aspx?mir=true&SpecNumber=" + dr["SpecNumber"] + "&txtlat=" + txtlat + "&txtlon=" + txtlon + "\" style=\"color: #000; text-decoration: none;\"><div class=\"";

                if (i % 2 != 0)
                {
                    homeList.Text += "listingContainer";
                }
                else
                {
                    homeList.Text += "listingContainerAlt";

                }

                homeList.Text += "\"><div style=\"float: right; height: auto; padding-top: 40px; vertical-align: middle;\"><img src=\"http://lib.mobimanage.com/ICONS/listingArrow.png\" />";
                homeList.Text += "</div>";

                try
                {
                    if (aImageSet.Tables[0].Rows[0]["Value"] != null)
                    {
                        homeList.Text += "<div style=\"float: left; height: 120px; margin-right: 2px; text-align: center; vertical-align: bottom; padding-top: 5px; width: 125px; margin-left: -20px;\">";
                        homeList.Text += "<img src=\"" + aImageSet.Tables[0].Rows[0]["Value"].ToString().Replace('\\', '/') + "\" style=\"vertical-align: top; max-height: 95px; width: 90px;\" />";
                        homeList.Text += "</div>";
                    }
                }
                catch (IndexOutOfRangeException e) { Console.Write(e.Message); }

                homeList.Text += "<h2 style=\"margin:2px 5px;\">" + dr["PlanName"].ToString() + "</h2>";
                homeList.Text += "<h3 style=\"margin:2px 5px;\">" + dr["SubDivisionName"].ToString() + "</h3>";
                homeList.Text += "<div class=\"listingDetailContainer\">$" + Convert.ToDouble(dr["SpecPrice"]).ToString("###,###") + " ";
                homeList.Text += "<br />" + dr["SpecStreet1"].ToString() + " <br />" +dr["SpecCity"].ToString() + ", " + dr["SpecState"].ToString() + " " + dr["SpecZip"].ToString() + "</div></div>";
               
            }


            if (aListingsDS.Tables[0].Rows.Count == 0)
                homeList.Text += "<div class=\"content\">I'm sorry, no move-in ready homes are available at this time.</div>";


            // PAGINTATION

            // Set Page number display
            int currPage = sRow + 1;
            pageTotal.Text = "PAGE " + currPage + " OF " + Math.Ceiling((Decimal)aListingsDS.Tables[0].Rows.Count / listingsPerPage);

            //Previous and Next links
            div1.Text = " | ";
            div2.Text = " | ";
            if (sRow >= 0)
            {
                prevBtn.Visible = false;
                prevBtn2.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
            }
            else
            {
                int prevRow = sRow - 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "sRow=[0-9]*", "", RegexOptions.IgnoreCase);
                prevBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
                prevBtn2.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
            }
            if (sRow == Math.Ceiling((Decimal)aListingsDS.Tables[0].Rows.Count / listingsPerPage) - 1)
            {
                nextBtn.Visible = false;
                nextBtn2.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
            }
            else
            {
                int nextRow = sRow + 1;
                String cleanQuery = Request.QueryString.ToString();
                cleanQuery = Regex.Replace(cleanQuery, "sRow=[0-9]*", "", RegexOptions.IgnoreCase);
                nextBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
                nextBtn2.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
            }


        }


#endregion
















#region FloorPlans "Show Default Data"



        private void DisplayPlans()
        {

            if (listingsPerPage == 0)
                listingsPerPage = 20;


            //CHECK FOR HOME PLANS
            String aSQL = "";


  
           
                aSQL = String.Format("SELECT DISTINCT a.PlanName,a.PlanID,a.PlanNumber, a.Stories, s.SubCity,s.SubState,s.SubDivisionName, s.SubDivisionID,a.BasePrice,a.BaseSqft,a.Bedrooms,a.Baths,a.HalfBaths FROM BHI_Plans a JOIN BHI_SubDivision s "
                      + "on s.subDivisionID=a.SubDivisionID JOIN BHI_Builder b ON b.BuilderID=s.BuilderID"
                      + " WHERE B.BUILDERID IN (SELECT DISTINCT BuilderID FROM Builder"
                      + " WHERE CORPORATIONID IN (SELECT DISTINCT CORPORATIONID"
                      + " FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101'))", listingTable);

       


            //   if (type.Length > 0)
            //       aSQL += " AND a.Type LIKE '%" + type.Replace("'", "") + "%'";
        
                  if (stories.Length > 0)
                      aSQL += " AND a.Stories = " + stories.Replace("'", "") + " ";

                  if (town.Length > 0)
                      aSQL += " AND s.SubCity = '" + town.Replace("'", "") + "'";

                  if (priceMin.Length > 0)
                      aSQL += " AND a.BasePrice >= " + priceMin.Replace("'", "") + " ";
                  if (priceMax.Length > 0)
                      aSQL += " AND a.BasePrice <= " + priceMax.Replace("'", "") + " ";

                  if (sqft.Length > 0)
                      aSQL += " AND a.BaseSqft >= " + sqft.Replace("'", "") + " ";
                  if (maxSqft.Length > 0)
                      aSQL += " AND a.BaseSqft <= " + maxSqft.Replace("'", "") + " ";

                  if (beds.Length > 0)
                      aSQL += " AND a.Bedrooms >= " + beds.Replace("'", "") + " ";
                  if (maxBeds.Length > 0)
                      aSQL += " AND a.Bedrooms <= " + maxBeds.Replace("'", "") + " ";

                  if (baths.Length > 0)
                      aSQL += " AND a.Baths >= " + baths.Replace("'", "") + " ";
                  if (maxBaths.Length > 0)
                      aSQL += " AND a.Baths <= " + maxBaths.Replace("'", "") + " ";

                  if (halfbaths.Length > 0)
                      aSQL += " AND a.HalfBaths >= " + halfbaths.Replace("'", "") + " ";
                  if (maxhalfBaths.Length > 0)
                      aSQL += " AND a.HalfBaths <= " + maxhalfBaths.Replace("'", "") + " ";
            // if we are sorting override sort by distance

                if (Request.QueryString["sort"] == null)
                {
                    aSQL += " ORDER BY a.PlanName ";

                }

                else if (Convert.ToInt32(Request.QueryString["sort"]) == 0)
                {
                    aSQL += " ORDER BY a.BasePrice ASC ";
                }

                else if (Convert.ToInt32(Request.QueryString["sort"]) == 1)
                {
                    aSQL += " ORDER BY a.BasePrice DESC ";
                }

         


            DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());


            if (aListingsDS.Tables.Count == 0)
            {

                return;

            }

            String sort = "";
            String hideListing = "";

            int startRow = sRow * listingsPerPage;
            int endRow = pageCount;

            if (endRow > aListingsDS.Tables[0].Rows.Count)
                endRow = aListingsDS.Tables[0].Rows.Count;



            //HIDE homes for load communites
            String smir = "";
            String sState = "";
            String stown = "";

            if (pageCount <= aListingsDS.Tables[0].Rows.Count)
                {
                    if (Request.QueryString["sort"] != null)
                        sort = "&sort=" + Request.QueryString["sort"].ToString();
                    if (Request.QueryString["mir"] != null)
                        smir = "&mir=" + Request.QueryString["mir"].ToString();
                    if (Request.QueryString["state"] != null)
                        sState = "&state=" + Request.QueryString["state"].ToString();
                   if (Request.QueryString["town"] != null)
                    stown = "&town=" + Request.QueryString["town"].ToString();


                    this.commBtn.Text = "<a name=\"target\" href=\"Listings.aspx?search=plans&pg=" + (pageCount + listingsPerPage) + sort + smir + sState + stown + "\" style=\"text-decoration:none;\"/><div id=\"comm_btn\" class=\"loadBtn\">LOAD MORE FLOORPLANS</div></a>";
                }
                else
                {
                    this.commBtn.Text = "<div id=\"comm_btn\" class=\"\" style=\"background-color:#f8f8f8;cursor:default;color:#666;display:none;\">Completed</div>";
                }
            

            //String currentCommunity = "";


            for (int i = 0; i < endRow; i++)
            {
                DataRow dr = aListingsDS.Tables[0].Rows[i];

                // Check for an image




                String imageQuery = "SELECT DISTINCT d.Value FROM BHI_SubDivision s JOIN BHI_Builder b ON b.BuilderID=s.BuilderID JOIN BHI_Plans c ON s.SubDivisionID=c.SubDivisionID " +
                                     "JOIN BHI_Plans h on h.PlanID=c.PlanID LEFT JOIN BHI_PlanImages d ON d.PlanID=h.PlanID WHERE B.BUILDERID IN (SELECT DISTINCT BuilderID " +
                                     "FROM Builder WHERE s.SubDivisionID ='" + dr["SubDivisionID"] + "' AND d.Type='ElevationImage' AND d.SequencePosition=1 AND CORPORATIONID IN (SELECT DISTINCT CORPORATIONID FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101'))";


                //  String imageQuery = "SELECT CLOUDPATH FROM OLTH_IMAGES WHERE ISPRIMARY = 1 AND LISTINGID = " + dr["specID"];
                DataSet aImageSet = SqlHelper.ExecuteDataset(connStr, CommandType.Text, imageQuery);




                Double Radius = 200.00;
                //   homeList.Text += Math.Round(Convert.ToDouble(dr["Dist"]), 2).ToString();








                if (txtlat != 0)
                {
                    if (Math.Round(Convert.ToDouble(dr["Dist"]), 2) > Radius)
                        hideListing = "Display:none;";
                    else
                        hideListing = "";

                }



                homeList.Text += "<a href=\"/FloorDetail.aspx?PlanNumber=" + dr["PlanNumber"] + "\" style=\"color: #000; text-decoration: none;" + hideListing + "\"><div class=\"";

                if (i % 2 != 0)
                {
                    homeList.Text += "listingContainer";
                }
                else
                {
                    homeList.Text += "listingContainerAlt";

                }

                homeList.Text += "\" style=\"min-height:88px;\"><div style=\"float: right; height: auto; padding-top: 37px; vertical-align: middle;\"><img src=\"http://lib.mobimanage.com/ICONS/listingArrow.png\" />";
                homeList.Text += "</div>";

                try
                {
                    if (aImageSet.Tables[0].Rows[0]["Value"] != null)
                    {
                        homeList.Text += "<div style=\"float: left; height: 85px; margin-right: 2px; text-align: center; vertical-align: bottom; padding-top: 5px; width: 125px; margin-left: -20px;\">";
                        homeList.Text += "<img src=\"" + aImageSet.Tables[0].Rows[0]["Value"].ToString().Replace('\\', '/') + "\" style=\"vertical-align: top; max-height: 85px; width: 90px;\" />";
                        homeList.Text += "</div>";
                    }
                }
                catch (IndexOutOfRangeException e) { Console.Write(e.Message); }

                homeList.Text += "<h2 style=\"margin:2px 5px;\">" + dr["PlanName"] + "</h2>";
                homeList.Text += "<h3 style=\"margin:2px 5px;\">" + dr["SubDivisionName"].ToString() + "</h3>";
                homeList.Text +=  dr["subCity"] + ", " + dr["subState"];
                homeList.Text += "<br />Starting from $" + Convert.ToDouble(dr["BasePrice"]).ToString("###,###");
               


                if (txtlat != 0 && Request.QueryString["sort"] == null)

                    homeList.Text += "<br />" + Convert.ToDouble(dr["Dist"].ToString()).ToString("##.##") + " miles";

                homeList.Text += "<br /></div>";











            }

            // PAGINTATION

            //If in GEO
            if (txtlat != 0)
            {
                pageTotal.Text = "PAGE 1 OF 1";
                prevBtn.Visible = false;
                prevBtn2.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
                nextBtn.Visible = false;
                nextBtn2.Visible = false;
            }
            else
            {
                // Set Page number display
                int currPage = sRow + 1;
                pageTotal.Text = "PAGE " + currPage + " OF " + Math.Ceiling((Decimal)aListingsDS.Tables[0].Rows.Count / listingsPerPage);

                //Previous and Next links
                div1.Text = " | ";
                div2.Text = " | ";
                if (sRow == 0)
                {
                    prevBtn.Visible = false;
                    prevBtn2.Visible = false;
                    div1.Visible = false;
                    div2.Visible = false;

                }
                else
                {
                    int prevRow = sRow - 1;
                    String cleanQuery = Request.QueryString.ToString();
                    cleanQuery = Regex.Replace(cleanQuery, "sRow=[0-9]*", "", RegexOptions.IgnoreCase);
                    prevBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
                    prevBtn2.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + prevRow;
                }
                if (sRow == Math.Ceiling((Decimal)aListingsDS.Tables[0].Rows.Count / listingsPerPage) - 1)
                {
                    nextBtn.Visible = false;
                    nextBtn2.Visible = false;
                    div1.Visible = false;
                    div2.Visible = false;
                }
                else
                {
                    int nextRow = sRow + 1;
                    String cleanQuery = Request.QueryString.ToString();
                    cleanQuery = Regex.Replace(cleanQuery, "sRow=[0-9]*", "", RegexOptions.IgnoreCase);
                    nextBtn.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
                    nextBtn2.NavigateUrl = Request.ServerVariables["URL"] + "?" + cleanQuery + Request.Form + "&sRow=" + nextRow;
                }
            }





        }

        #endregion












        
    }
}