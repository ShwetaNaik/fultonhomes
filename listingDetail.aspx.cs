﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Globalization;

namespace MobileSite
{
    public partial class listingDetail : System.Web.UI.Page
    {
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String listingTable = "BHI_Specs";
        string listID = string.Empty;
        // int GEOOn = 1;
        String SubTel = "";
        Double txtlat = 0;
        Double txtlon = 0;
        private String SpecNumber = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["showMap"] = true;
                Session["showFav"] = true;

                if (Request.QueryString["listID"] != null)
                    listID = Request.QueryString["listID"].ToString();
                if (Request.QueryString["txtlat"] != null)
                    txtlat = Convert.ToDouble(Request.QueryString["txtlat"]);
                if (Request.QueryString["txtlon"] != null)
                    txtlon = Convert.ToDouble(Request.QueryString["txtlon"]);


                if (!string.IsNullOrEmpty(Request.QueryString["SpecNumber"]))
                    SpecNumber = Request.QueryString["SpecNumber"];


                DisplayData();


            }

            this.mEstLabel.Text = this.calc.GetTRextResult();



        }

        private void DisplayData()
        {
            try
            {


                //Catch listingID of 0 and redirect to home page
                if (listID.Length==0 && SpecNumber.Length == 0)
                    Response.Redirect("/listings.aspx");

                // Start query
                String query = String.Format("SELECT Distinct(e.SpecStreet1),s.SubDivisionName,s.SubDescription," +
                                             "e.SpecHalfBaths, e.SpecStories, s.SalesOffice_Email,s.SalesOffice_Hours," +
                                             " s.SalesOffice_Phone, e.SpecZip, e.SpecCity,e.SpecState,e.SpecSqFt,e.SpecBaths," +
                                             "e.SpecBedrooms, e.SpecID,e.SpecMoveInDate,e.SpecGarage,e.SpecPrice, s.SalesOffice_Phone, a.PlanName, a.PlanID FROM BHI_Plans a Inner Join BHI_Specs e on a.planID=e.planID JOIN BHI_SubDivision s "
                                             +
                                             " on s.subDivisionID=a.SubdivisionID JOIN BHI_Builder b ON b.BuilderID=s.BuilderID"
                                             + " WHERE B.BUILDERID IN (SELECT DISTINCT BuilderID FROM Builder"
                                             + " WHERE 1=1 ");
                if (listID.Length > 0)
                {
                   query += String.Format(" AND SpecID = '" + listID + "'");
                }

                if (SpecNumber.Length>0)
                {
                    query += String.Format(" AND SpecNumber='"+ SpecNumber+"' ");
                }
                query += String.Format(" AND CORPORATIONID IN (SELECT DISTINCT CORPORATIONID"
                     + " FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101'))");

                DataSet getListing = SqlHelper.ExecuteDataset(connStr, CommandType.Text, query);

                DataRow dr = getListing.Tables[0].Rows[0];
                DateTime oldDate = DateTime.Parse(dr["SpecMoveInDate"].ToString());

                String halfBath;

                if (!string.IsNullOrEmpty(dr["SalesOffice_Phone"].ToString()))
                {
                    SubTel = dr["SalesOffice_Phone"].ToString();
                }

                if (!string.IsNullOrEmpty(dr["SpecPrice"].ToString()))
                {
                    this.calc.MortgagePrice = dr["SpecPrice"].ToString();
                }


                String FPQuery = String.Format("SELECT BHI_PlanImages.Value,BHI_PlanImages.Title, BHI_Plans.PlanName,BHI_Plans.BasePrice,BHI_Plans.Bedrooms,BHI_Plans.Baths, BHI_Plans.BaseSqft, BHI_Plans.HalfBaths FROM BHI_Plans INNER JOIN BHI_PlanImages ON BHI_Plans.PlanID=BHI_PlanImages.PlanID WHERE BHI_Plans.PlanID='" + dr["PlanID"] + "' AND BHI_PlanImages.Type='FloorPlanImage' ;", listID);
                DataSet Fpd = SqlHelper.ExecuteDataset(connStr, CommandType.Text, FPQuery);


                if (Fpd.Tables[0].Rows.Count > 0)
                {

                    if (Fpd.Tables[0].Rows[0]["Value"].ToString().Length > 0)
                    {

                        FPList.Text += "<img src=\"" + Fpd.Tables[0].Rows[0]["Value"].ToString() + "\"  style=\"text-decoration:none;max-width:280px;\" alt=" + Fpd.Tables[0].Rows[0]["Title"].ToString() + "\" />";

                    }


                }





                if (!string.IsNullOrEmpty(dr["SpecHalfBaths"].ToString()) && dr["SpecHalfBaths"].ToString() == "1")
                {
                    halfBath = ".5";

                }

                else
                {
                    halfBath = "";
                }

                String googAddr = "http://maps.google.com/maps?hl=en&q=" + dr["SpecStreet1"].ToString() + "+" + dr["SpecCity"].ToString() + "+" + dr["SpecState"].ToString() + "+" + dr["SpecZip"].ToString();






                if (!string.IsNullOrEmpty(dr["SpecPrice"].ToString()))
                {
                    listingInfoTop.Text +=
                        "<div class=\"ListingDetailContainer\" style=\"width:100%;margin:3px 15px;text-align:left;\"><b>" +
                        dr["PlanName"] +
                        "</b><br /><span class=\"startingFrom\"> " +
                        Convert.ToDouble(dr["SpecPrice"]).ToString("C0", CultureInfo.CurrentCulture) + "</span>";
                }

                double mEst = 0;

                try
                {

                    mEst = HomeFinancials.GetMonthlyMortgagePayment(4, 0, Convert.ToDouble(dr["SpecPrice"]), 0, 0, 30);

                }
                catch (Exception ex) { }



                //  listingInfo.Text += "<br /> ** Monthly Payment <a href=\"Javascript:opPanel('#MCPanel', '#mcImg')\" onClick=\"Javascript:window.scrollTo(100,500);\" style=\"text-decoration:none;\">" + mEst.ToString("C0", CultureInfo.CurrentCulture) + "</a><br />";

                listingInfoBot.Text += dr["SpecBedrooms"] + " Beds / " + dr["SpecBaths"] + halfBath + " Bath<br />";



                listingInfoBot.Text += "<a target=\"_blank\" href=\"" + googAddr + "\">" + dr["SpecStreet1"] + "<br /> " + dr["SpecCity"] + ", " + dr["SpecState"].ToString() + " " + dr["SpecZip"].ToString() + "</a></div><div class=\"clear\"></div>\n";
                listingInfoBot.Text += "</div>";



                //  comm_info_l.Text += dr["SubDescription"].ToString();


                //   comm_info_l.Text  += dr["SalesOffice_Phone"].ToString();

                if (!string.IsNullOrEmpty(dr["SalesOffice_Phone"].ToString()))
                {
                    comm_info_l.Text = "<a href=\"tel:" +
                                       String.Format("{0:(###) ###-####}", Convert.ToDouble(dr["SalesOffice_Phone"])) +
                                       "\" class=\"submitbutton\" style=\"background-color:#660000;text-decoration:none;\">" +
                                       String.Format("{0:(###) ###-####}",
                                           Convert.ToDouble(dr["SalesOffice_Phone"].ToString())) + "</a>";
                }



                //SHARE COMMUNITIY

                String ShareUrl = Convert.ToString(System.Web.HttpContext.Current.Request.Url);

                share_comm.Text += "<a target=\"_blank\" href=\"http://www.facebook.com/sharer/sharer.php?u=" + ShareUrl + "\"><img src=\"http://1627.mobimanage.com/IMAGES/facebook.png\" width=\"31\" height=\"31\" alt=\"Share this Community on Facebook!\" /></a> &nbsp;&nbsp;&nbsp;";

                share_comm.Text += "<a target=\"_blank\" href=\"https://twitter.com/share\" class=\"twitter-share-button\" data-url=\"" + ShareUrl + "\" data-text=\"Check out this community\" data-via=\"fultonhomes\" data-count=\"none\">Tweet</a> &nbsp;&nbsp;&nbsp;" +
                                  "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";

                share_comm.Text += "<a href=\"mailto: ?subject=Fulton Homes Community Information&body=I found this great FULTON Homes Community on their mobile website.   \r\n\r\t\n" + ShareUrl + "\"><img src=\"http://1627.mobimanage.com/IMAGES/mail.png\" width=\"31\" height=\"23\" alt=\"\" /></a>";




                // Check for an image
                String imageQuery = "SELECT DISTINCT d.Value FROM BHI_SubDivision s JOIN BHI_Builder b ON b.BuilderID=s.BuilderID JOIN BHI_Plans c ON s.SubDivisionID=c.SubDivisionID " +
                                    "JOIN BHI_Specs h on h.PlanID=c.PlanID LEFT JOIN BHI_SpecImages d ON d.SpecID=h.SpecID WHERE B.BUILDERID IN (SELECT DISTINCT BuilderID " +
                                    "FROM Builder WHERE d.SpecID ='" + dr["SpecID"] + "' AND CORPORATIONID IN (SELECT DISTINCT CORPORATIONID FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101'))";
                DataSet aImageSet = SqlHelper.ExecuteDataset(connStr, CommandType.Text, imageQuery);

                if (aImageSet.Tables[0].Rows.Count == 0)
                    imgs.Visible = false;


                if (aImageSet.Tables[0].Rows.Count > 0)
                {

                    if (aImageSet.Tables[0].Rows[0]["Value"].ToString() != "")
                    {

                        listingImages.Text += "<img src=\"" + aImageSet.Tables[0].Rows[0]["Value"].ToString().Replace('\\', '/') + "\" style=\"width:280px;\" alt=\"" +
                            aImageSet.Tables[0].Rows[0]["Value"] + "\" />";

                    }

                }
                // initialCaption.Text = aImageSet.Tables[0].Rows[0]["Value"].ToString();

                // Display the sales info

                // salesList.Text += "Contact: " + dr["SalesOffice_Phone"] + "<br />Phone: <a href=\"tel:" + dr["SalesOffice_Phone"] + "\">" + dr["SalesOffice_Phone"] +
                //"</a><br />Email: <a href=\"mailto:" + dr["SalesOffice_Email"] + "?subject=Info Request for move-in ready home \">" + dr["SalesOffice_Email"] + "</a>";


                // Get community info for the home and populate video, amenities, School info, and directions


                //  String communityQuery = String.Format("SELECT * FROM OLTH_COMMUNITIES WHERE COMMUNITY = '{0}' ORDER BY COMMUNITY", dr["Community"]);
                //  DataSet communityDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, communityQuery);

                // DataRow cDr = communityDS.Tables[0].Rows[0];

                //  amenitiesList.Text += cDr["Ammenities"];
                //   dirList.Text += cDr["Directions"];
                //  schoolList.Text += "District: " + cDr["School_DistrictName"] + "<br />School: " + cDr["School_DistrictName"] + "<br />" + 
                //    cDr["SchoolLink"] + "</a>";

                //    if (cDr["CommunityVideoLink"].ToString().Length == 0 || !cDr["CommunityVideoLink"].ToString().Contains("youtube"))
                //    vidContainer.Visible = false;

                //  else
                //  {
                //     vidList.Text += "<iframe width=\"280\" height=\"210\" src=\"http://www.youtube.com/embed/" + 
                //       cDr["CommunityVideoLink"].ToString().Replace("http://www.youtube.com/watch?v=", "") + "\" frameborder=\"0\" ></iframe>";
                //   }

            }
            catch (Exception ex)
            {


            }
        }




    }
}