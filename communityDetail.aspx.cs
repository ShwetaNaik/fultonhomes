﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;


namespace MobileSite
{
    public partial class communityDetail : System.Web.UI.Page
    {

        public string listID = string.Empty;
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String listingTable = "BHI_SubDivision";
        protected String SubTel = "";
        public String subDivisionNumber = string.Empty;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["showMap"] = true;
                Session["showFavs"] = true;

                if (!string.IsNullOrEmpty(Request.QueryString["listID"]))
                    listID = Request.QueryString["listID"];

                if (!string.IsNullOrEmpty(Request.QueryString["SubDivisionNumber"]))
                    subDivisionNumber = Request.QueryString["SubDivisionNumber"];


                DisplayData();

            }


            this.mEstLabel.Text = this.calc.GetTRextResult();


        }

        protected void DisplayData()
        {
            try
            {


                if (listID.Length == 0 && subDivisionNumber.Length == 0)
                    Response.RedirectLocation = "/communityListings.aspx";



                // We have a list ID, check to make sure it has data
                System.Text.StringBuilder aSQL = new System.Text.StringBuilder();


                aSQL.Append(
                    String.Format(
                        "SELECT * FROM BHI_SubDivision a JOIN BHI_Plans b ON a.SubDivisionID=b.SubDivisionID WHERE 1=1 "));
                if (listID.Length>0)
                {
                    aSQL.Append(
                        String.Format(
                            " AND  a.SubDivisionID ='" + listID + "'" )); 
                          
                }
                if (subDivisionNumber.Length>0)
                {
                    aSQL.Append(
                        String.Format(
                            " AND  a.subDivisionNumber ='" + subDivisionNumber + "'"));

                }
                   aSQL.Append(
                        String.Format(" ORDER BY SubDivisionName", listingTable, listID));

                DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());

                // Make sure the community exists
                if (aListingsDS.Tables[0].Rows.Count == 0)
                    Response.RedirectLocation = "/communityListings.aspx";


                DataRow dr = aListingsDS.Tables[0].Rows[0];


                if (!string.IsNullOrEmpty(dr["PriceLow"].ToString()))
                {
                    this.calc.MortgagePrice = dr["PriceLow"].ToString();
                }


                // Handle the community images

                String ImageQuery = String.Format("SELECT a.Value,a.Caption FROM BHI_PlanImages a JOIN BHI_Plans b ON a.PlanID=b.PlanID JOIN BHI_Subdivision c ON c.SubDivisionID=b.SubDivisionID WHERE c.SubdivisionID ='" + dr["SubDivisionID"] + "' AND a.Type='ElevationImage' AND a.SequencePosition=1", listID);
                DataSet Fpdss = SqlHelper.ExecuteDataset(connStr, CommandType.Text, ImageQuery);
                for (int i = 0; i < Fpdss.Tables[0].Rows.Count; i++)
                {


                    communityImages.Text += "<img id=\"comm_img\" src=\"" + Fpdss.Tables[0].Rows[i]["Value"].ToString().Replace('\\', '/') +
                      "\" style=\"width:280px;height:139px;\" alt=\"\" />";

                }

                if (!string.IsNullOrEmpty(dr["SalesOffice_Phone"].ToString()))
                {
                    SubTel = dr["SalesOffice_Phone"].ToString();
                }

                // midbar.Text += "<a href=\"/listings.aspx?mir=true&community="  + dr["SubDivisionName"].ToString() + "\" ><button type=\"button\" class=\"submitbutton\">MOVE-IN READY</button></a>";
                //   midbar.Text += "<a href=\"tel:" + SubTel + "\" ><button type=\"button\" class=\"resetbutton\">CONTACT FULTON</button></a>";

                //if (dr["phone"].ToString().Length > 0 )
                //  headButtons.Text += "<a href=\"tel:" + dr["phone"] + "')\"><div class=\"smallBtn\" style=\"float: right; margin-right 15px;\">Contact</div></a>";

                //Headline bar



                String FPQuery = String.Format("SELECT BHI_PlanImages.Value,BHI_Plans.PlanName,BHI_Plans.PlanID,BHI_Plans.PlanNumber, BHI_Plans.BasePrice,BHI_Plans.Bedrooms,BHI_Plans.Baths, BHI_Plans.BaseSqft, BHI_Plans.HalfBaths FROM BHI_Plans INNER JOIN BHI_PlanImages ON BHI_Plans.PlanID=BHI_PlanImages.PlanID WHERE BHI_Plans.SubdivisionID={0} AND BHI_PlanImages.Type='ElevationImage' AND BHI_PlanImages.SequencePosition=1 ORDER BY BHI_Plans.PlanName;", dr["SubdivisionID"]);
                DataSet Fpd = SqlHelper.ExecuteDataset(connStr, CommandType.Text, FPQuery);

                // Floor detail

                for (int i = 0; i < Fpd.Tables[0].Rows.Count; i++)
                {

                    FPList.Text += "<a href=\"FloorDetail.aspx?plan=true&planNumber=" + Fpd.Tables[0].Rows[i]["PlanNumber"].ToString() + "\" style=\"color: #000; text-decoration:none;\">";
                    // even or odd  to alternate

                    if (i % 2 == 0)
                    {

                        FPList.Text += "<div class=\"listingContainer\" style=\"width:auto;min-height:68px;\">";

                    }
                    else
                    {
                        FPList.Text += "<div class=\"listingContainerAlt\" style=\"width:auto;min-height:68px;\">";
                    }

                    String halfBath;
                    if (Fpd.Tables[0].Rows[i]["halfBaths"].ToString() == "1")
                    {
                        halfBath = ".5";

                    }

                    else
                    {
                        halfBath = "";
                    }

                    var img = Fpd.Tables[0].Rows[i]["Value"].ToString();


                    FPList.Text += "<div style=\"float: right; height: auto; padding-top: 35px; vertical-align: middle;\"><img src=\"http://lib.mobimanage.com/ICONS/listingArrow.png\" /></div>";
                    FPList.Text += "<div style=\"float: left; height: auto;  padding:3px 10px;\"><img src=\"" + img + "\" Border=\"0\" width=\"75\" style=\"margin-top:0px;\" /></div>";
                    FPList.Text += "<h2 style='margin:3px 0px;'>" + Fpd.Tables[0].Rows[i]["PlanName"].ToString() + "</h2>";
                    FPList.Text += "Starting at " + Convert.ToDouble(Fpd.Tables[0].Rows[i]["BasePrice"]).ToString("C0", CultureInfo.CurrentCulture) + "<br />";
                    //   FPList.Text = "** Monthly Payment " + mEst.ToString("C0", CultureInfo.CurrentCulture) + "<br />";
                    FPList.Text += Fpd.Tables[0].Rows[i]["Bedrooms"].ToString() + " Beds \\ " + Fpd.Tables[0].Rows[i]["Baths"].ToString() + halfBath + " Baths \\ " + Fpd.Tables[0].Rows[i]["BaseSqft"].ToString() + " sqft";
                    FPList.Text += "</div></a>";

                }











                double mEst = 0;

                try
                {
                    //     mEst = HomeFinancials.MortgageEstimate;
                    //  mEst = HomeFinancials.GetMonthlyMortgagePayment(4, 0, Convert.ToDouble(dr["PriceLow"]), 0, 0, 30);
                }
                catch (Exception ex) { }


                communityInfoTop.Text += "<div class=\"ListingDetailContainer\" style=\"margin:3px 15px;text-align:left;\"><b>" + dr["SubDivisionName"] + "</b><br /><span class=\"startingFrom\">From: " + Convert.ToDouble(dr["PriceLow"]).ToString("C0", CultureInfo.CurrentCulture) + "</span><br />";


                //  communityInfo.Text += "** Monthly Payment <a href=\"Javascript:opPanel('#MCPanel', '#mcImg')\" onClick=\"Javascript:window.scrollTo(100,500);\" style=\"text-decoration:none;\">" + mEst.ToString("C0", CultureInfo.CurrentCulture) + "</a><br />";

                communityInfoBot.Text += "<div class=\"clear\"></div>";

                // Print address and community logo
                //  communityInfo.Text += "<img src=\"" + "\" style=\"float: right; with: 110px;\" />";
                String googAddr = "http://maps.google.com/maps?hl=en&q=" + dr["SalesOffice_Street1"].ToString() + "+" + dr["SalesOffice_City"].ToString() + "+" + dr["SalesOffice_State"].ToString() + "+" + dr["SalesOffice_Zip"].ToString();


                communityInfoBot.Text += "<a target=\"_blank\" href=\"" + googAddr + "\">" + dr["SalesOffice_Street1"] + "<br />" + dr["SalesOffice_City"] + ", " + dr["SalesOffice_State"].ToString() + " " + dr["SalesOffice_Zip"] + "</a></div><div class=\"clear\"></div>\n";

                if (!string.IsNullOrEmpty(dr["SalesOffice_Phone"].ToString()))
                {
                    comm_info_l.Text = "<a href=\"tel:" +
                                       String.Format("{0:(###) ###-####}", Convert.ToDouble(dr["SalesOffice_Phone"])) +
                                       "\" class=\"submitbutton\" style=\"background-color:#660000;text-decoration:none;\">" +
                                       String.Format("{0:(###) ###-####}",
                                           Convert.ToDouble(dr["SalesOffice_Phone"].ToString())) + "</a>";
                }



                //  String hours="";
                // hours = dr["SalesOffice_Hours"].ToString().Replace(";", "<br />");
                //  hours = dr["SalesOffice_Hours"].ToString().Replace(",", "<br />");
                //  hours = dr["SalesOffice_Hours"].ToString().Replace("/t", "<br />");
                //  hours_locale.Text = hours;
                // Look Up and print the image list for the community when data is available



                //SHARE COMMUNITIY

                String ShareUrl = Convert.ToString(System.Web.HttpContext.Current.Request.Url);

                share_comm.Text += "<a  target=\"_blank\" href=\"http://www.facebook.com/sharer/sharer.php?u=" + ShareUrl + "\"><img src=\"http://1627.mobimanage.com/IMAGES/facebook.png\" width=\"31\" height=\"31\" alt=\"Share this Community on Facebook!\" /></a> &nbsp;&nbsp;&nbsp;";

                share_comm.Text += "<a target=\"_blank\"  href=\"https://twitter.com/share\" class=\"twitter-share-button\" data-url=\"" + ShareUrl + "\" data-text=\"Check out this community\" data-via=\"fultonhomes\" data-count=\"none\">Tweet</a> &nbsp;&nbsp;&nbsp;" +
                                  "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";

                share_comm.Text += "<a href=\"mailto: ?subject=FULTON Homes Community Information&body=I found this great FULTON Homes Community on their mobile website.   \r\n\r\t\n" + ShareUrl + "\"><img src=\"http://1627.mobimanage.com/IMAGES/mail.png\" width=\"31\" height=\"23\" alt=\"\" /></a>";





                // Handle the drop downs for the community
                //    amenitiesList.Text += dr["Ammenities"];
                //   dirList.Text += dr["Directions"];
                //   schoolList.Text += "District: " + dr["SchoolDistrict"] + "<br />School: " + dr["SchoolName"] + "<br /><a href=\"" + dr["SchoolLink"] + "\">" + dr["SchoolLink"] + "</a>";

                // if (dr["CommunityVideoLink"].ToString().Length == 0 || !dr["CommunityVideoLink"].ToString().Contains("youtube"))
                //   vidContainer.Visible = false;

                //  else
                // {
                //vidList.Text += "<iframe width=\"280\" height=\"210\" src=\"http://www.youtube.com/embed/" + dr["CommunityVideoLink"].ToString().Replace("http://www.youtube.com/watch?v=", "") +
                //     "\" frameborder=\"0\" ></iframe>";
                // }

            }
            catch (Exception)
            {


            }

        }
    }
}