﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MobileSite
{
    public partial class photos : System.Web.UI.Page
    {
        protected static String strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["PortalConn"].ToString();
        protected String aDomainID = System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString();
        protected int os5 = 0;
        protected int imgID = -1;
        protected String galleryURL = "http://" + System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString() + ".mobimanage.com/GALLERY/IMG/";

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack){

                if (Request.QueryString["os5"] != null)
                    os5 = int.Parse(Request.QueryString["os5"]);
                if (Request.QueryString["imageID"] != null)
                    imgID = int.Parse(Request.QueryString["imageID"]);

                DisplayData();
            }
        }

        protected void DisplayData()
        {
            String qry = "SELECT * FROM mobiGallery WHERE DOMAINID = " + aDomainID +" ORDER BY SORTORDER";
            DataSet aDs = SqlHelper.ExecuteDataset(strConnection, CommandType.Text, qry);

            if (os5 == 1)
            {
                bbOS.Visible = false;
                slideJS.Visible = false;
                isBBOS(aDs);
            }
            else
            {
                notBBOS(aDs);
            }
        }

        //This method prints out data for a bb user
        protected void isBBOS(DataSet data){

            if (data.Tables[0].Rows.Count == 1)
                singlePhoto(data);

            else
                galLabel.Text = gallery(data);

        }

        //This method compiles data for a regular user
        protected void notBBOS(DataSet data){

            if (data.Tables[0].Rows.Count == 1)
                singlePhoto(data);

            else
            {
                galLabel.Text += "<div id=\"jsEnabled2\" style=\"display: none;\">\n" + slidy(data) + "\n</div>\n\n";
                galLabel.Text += "<div id=\"jsDisabled2\">\n" + gallery(data) + "\n</div>\n\n";
            }
        }

        //This method prints out data for the javascript slideshow portion
        protected String slidy(DataSet data)
        {

            String build = "";
            String firstDescr = "";

            build += "<link rel=\"stylesheet\" type=\"text/css\" href=\"/GALLERY/styles.css\" />\n\t<div id=\"imgContainer\">\n\t<div id=\"imagegallery\">";

            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {

                DataRow dr = data.Tables[0].Rows[i];

                var webClient = new WebClient();
                byte[] imageBytes = webClient.DownloadData(galleryURL + dr["fileName"].ToString());
                MemoryStream stream = new MemoryStream(imageBytes);
                System.Drawing.Image img = System.Drawing.Image.FromStream(stream);
                stream.Close();



                int width = img.Width;
                int height = img.Height;

                double modifier = ((double)270 / (double)width);
                double adjHeight = (double)height * modifier;

                if (i == 0)
                {
                    build += String.Format("<img src=\"{0}{1}\" width=\"270px\" height=\"{2}\" class=\"iAmImg\" alt=\"{3}\" />\n", galleryURL,
                        dr["fileName"], adjHeight, dr["description"]);
                    firstDescr = dr["description"].ToString();
                }
                else
                    build += String.Format("<img src=\"{0}{1}\" style=\"display: none;\" width=\"270px\" height=\"{2}\" class=\"iAmImg\" alt=\"{3}\" />\n", galleryURL,
                        dr["fileName"], adjHeight, dr["description"]);
            }

            build += "</div>\n<a id=\"prev\" href=\"#\"><span class=\"P-arrow previous\"></span></a><a id=\"next\" href=\"#\"><span class=\"N-arrow next\" style=\"margin-right:15px;\"></span></a>\n</div><div class=\"galDescr\" id=\"galDescr\">"
                + firstDescr + "</div>";

            return build;
        }

        //This method prints out data for the no JS gallery
        protected String gallery(DataSet data){

            String build = "";

            if (imgID > -1)
            {
                build += String.Format("<a href=\"{0}\">All</a> | ", Request.ServerVariables["URL"]);

                if (imgID != 0)
                    build += String.Format("<a href=\"{0}?imageID={1}\">Previous Image</a>", Request.ServerVariables["URL"], imgID - 1);

                if (imgID != 0 && imgID != data.Tables[0].Rows.Count - 1)
                    build += " | ";

                if (imgID <= data.Tables[0].Rows.Count - 1)
                    build += String.Format("<a href=\"{0}?imageID={1}\">Next Image</a>", Request.ServerVariables["URL"], imgID + 1);

                build += "<br /><br />";

                DataRow dr = data.Tables[0].Rows[imgID];
                build += String.Format("<img src=\"{0}{1}\" alt=\"\" width=\"280px\" />", galleryURL, dr["fileName"]);
            }

            else
            {
                build += "<div class=\"photoContainer\">";

                for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = data.Tables[0].Rows[i];
                    build += String.Format("<a href=\"{0}?imageID={1}\"><img src=\"{2}{3}\" alt=\"\" width=\"100\" height=\"60\" /></a>", Request.ServerVariables["URL"], 
                        i, galleryURL, dr["fileName"]);
                }

                build += "</div>";
            }


            return build;
        }

        //This method prints out data for a single photo
        protected void singlePhoto(DataSet data)
        {
            DataRow dr = data.Tables[0].Rows[0];
            galLabel.Text = String.Format("<img src=\"{0}{1}\" alt=\"\" width=\"100\" height=\"60\" />", galleryURL, dr["filename"]);
        }
    }
}