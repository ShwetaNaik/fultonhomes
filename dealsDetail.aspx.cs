﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MobileSite
{
    public partial class dealsDetail : System.Web.UI.Page
    {
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String dealsTable = Utility.GetMobiSiteSettings("OffersTable");
        protected int listingsPerPage = Utility.GetMobiSiteSettingsInt("NumListingRows");
        int oID = 0;
        int dealsDetailLayout = 0; // set up in DB and util to enter a layout number 0 is standard
        int iconSize = 37; // set in DB to allow a new icon size vairable for listings Icons
        String emailIcon = "http://lib.mobimanage.com/ICONS/email.png"; //Add DB field and util, default given
        String webIcon = "http://lib.mobimanage.com/ICONS/web.png"; //Add DB field and util, default given
        String mapIcon = "http://lib.mobimanage.com/ICONS/map.png"; //Add DB field and util, default given
        String phoneIcon = "http://lib.mobimanage.com/ICONS/phone.png"; //Add DB field and util, default given

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["oID"] != null)
                    oID = int.Parse(Request.QueryString["oID"]);
                if (Request.QueryString["CNM"] != null)
                {
                    Page.Title = Request.QueryString["CNM"];

                }
                DisplayData();
            }
        }

        private void DisplayData()
        {
            //Catch listingID of 0 and redirect to home page
            if (oID == 0)
                Response.Redirect("/Default.aspx");

            // Start query
            String query = String.Format("SELECT * FROM {0} WHERE offerID = {1} ORDER BY Title", dealsTable, oID.ToString());
            DataSet getListing = SqlHelper.ExecuteDataset(connStr, CommandType.Text, query);

            DataRow drPage = getListing.Tables[0].Rows[0];

            switch (dealsDetailLayout)
            {
                case 0:
                    dealsDetailInfo.Text = DealsDetailLayouts.layout0(drPage, iconSize, emailIcon, webIcon, mapIcon, phoneIcon);
                    break;
            }


        }
    }
}