﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net.Mail;

namespace MobileSite
{
    public partial class FloorDetail : System.Web.UI.Page
    {
        public static string listID = string.Empty;
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String listingTable = "BHI_Plans";
        protected static String subID = "";
        String SubTel = "";
        private String PlanNumber = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["showMap"] = true;

                if (Request.QueryString["listID"] != null)
                    listID = Request.QueryString["listID"];

                if (!string.IsNullOrEmpty(Request.QueryString["planNumber"]))
                    PlanNumber = Request.QueryString["planNumber"];


                DisplayData();

                //  Session["showMap"] = false;
                //  Session["showFavs"] = false;

            }


        }




        protected void DisplayData()
        {
            try
            {


                if (listID.Length == 0 && PlanNumber.Length==0)
                    Response.RedirectLocation = "/communityListings.aspx";

                // We have a list ID, check to make sure it has data
                System.Text.StringBuilder aSQL = new System.Text.StringBuilder();
              //  aSQL.Append(String.Format("PlanNumber ='" + listID + "' ORDER BY PlanName", listingTable, listID));

                aSQL.Append(
                 String.Format(
                     "SELECT * FROM BHI_Plans a JOIN BHI_Subdivision b ON a.SubDivisionID=b.SubDivisionID WHERE 1=1 "));
                if (listID.Length>0)
                {
                    aSQL.Append(
                        String.Format(
                            " AND  PlanID ='" + listID + "'"));

                }
                if (PlanNumber.Length>0)
                {
                    aSQL.Append(
                        String.Format(
                            " AND  PlanNumber ='" + PlanNumber + "'"));

                }
                aSQL.Append(
                     String.Format(" ORDER BY PlanName"));


                DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());



                // Make sure the community exists
                if (aListingsDS.Tables[0].Rows.Count == 0)
                    Response.RedirectLocation = "/communityListings.aspx";


                DataRow dr = aListingsDS.Tables[0].Rows[0];

                subID = dr["SubDivisionID"].ToString();

                if (!string.IsNullOrEmpty(dr["BasePrice"].ToString()))
                {
                    this.calc.MortgagePrice = dr["BasePrice"].ToString();
                }

                // Handle the community images

                String ImageQuery = String.Format("SELECT a.Value,a.Title FROM BHI_PlanImages a JOIN BHI_Plans b ON a.PlanID=b.PlanID JOIN BHI_Subdivision c ON c.SubDivisionID=b.SubDivisionID WHERE a.PlanID ='" + dr["PlanID"] + "' AND a.Type='ElevationImage' ", listID);
                DataSet Fpdss = SqlHelper.ExecuteDataset(connStr, CommandType.Text, ImageQuery);

                for (int i = 0; i < Fpdss.Tables[0].Rows.Count; i++)
                {


                    communityImages.Text += "<img src=\"" + Fpdss.Tables[0].Rows[i]["Value"].ToString().Replace('\\', '/') +
                      "\" style=\"width:280px; height:139px; \" alt=\"" + Fpdss.Tables[0].Rows[i]["Title"].ToString() + "\" />";

                }

                //if (dr["phone"].ToString().Length > 0 )
                //  headButtons.Text += "<a href=\"tel:" + dr["phone"] + "')\"><div class=\"smallBtn\" style=\"float: right; margin-right 15px;\">Contact</div></a>";

                //Headline bar


                String FPQuery = String.Format("SELECT BHI_PlanImages.Value,BHI_PlanImages.Title, BHI_Plans.PlanName,BHI_Plans.BasePrice,BHI_Plans.Bedrooms,BHI_Plans.Baths, BHI_Plans.BaseSqft, BHI_Plans.HalfBaths FROM BHI_Plans INNER JOIN BHI_PlanImages ON BHI_Plans.PlanID=BHI_PlanImages.PlanID WHERE BHI_Plans.PlanID='" + dr["PlanID"] + "' AND BHI_PlanImages.Type='FloorPlanImage' ;", listID);
                DataSet Fpd = SqlHelper.ExecuteDataset(connStr, CommandType.Text, FPQuery);


                if (Fpd.Tables[0].Rows.Count > 0)
                {

                    if (Fpd.Tables[0].Rows[0]["Value"].ToString().Length > 0)
                    {

                        FPList.Text += "<img src=\"" + Fpd.Tables[0].Rows[0]["Value"].ToString() + "\"  style=\"max-width:280px;padding:8px;\" border=\"0\" />";

                    }


                }

                if (!string.IsNullOrEmpty(dr["SalesOffice_Phone"].ToString()))
                {
                    SubTel = dr["SalesOffice_Phone"].ToString();
                }

                if (!string.IsNullOrEmpty(dr["BasePrice"].ToString()))
                {
                    communityInfo.Text += "<div style=\"float:left;\"><h2>" + dr["PlanName"] +
                                          "</h2><span class=\"startingFrom\">Starting From: " +
                                          Convert.ToDouble(dr["BasePrice"]).ToString("C0", CultureInfo.CurrentCulture) +
                                          "</span><br />";
                }

                // Print address and community logo

                double mEst = 0;

                try
                {
                    if (!string.IsNullOrEmpty(dr["PriceLow"].ToString()))
                    {
                        mEst = HomeFinancials.GetMonthlyMortgagePayment(4, 0, Convert.ToDouble(dr["PriceLow"]), 0, 0, 30);
                    }
                }
                catch (Exception ex) { }

                communityInfo.Text += "** Monthly Payment <a href=\"Javascript:opPanel('#MCPanel', '#mcImg')\" onClick=\"Javascript:window.scrollTo(100,500);\" style=\"text-decoration:none;\">" + mEst.ToString("C0", CultureInfo.CurrentCulture) + "</a><br />";

                if (!string.IsNullOrEmpty(dr["Garage"].ToString()))
                {
                    communityInfo.Text += dr["Bedrooms"] + " Beds / " + dr["Baths"] + " Baths / " +
                                          double.Parse(dr["Garage"].ToString()).ToString("#0") + " Garage \n";
                }

                communityInfo.Text += dr["BaseSQFT"] + " Sqft";



                if (!string.IsNullOrEmpty(dr["Stories"].ToString()) && Convert.ToDouble(dr["Stories"]) > 0)
                {

                    communityInfo.Text += " / " + Convert.ToDouble(dr["Stories"]) + " Story";

                }


                communityInfo.Text += "</div><div class=\"clear\"></div>\n";


                comm_info_l.Text = dr["Description"].ToString();

                //  String hours = "";
                // hours = dr["SalesOffice_Hours"].ToString().Replace(";", "<br />");
                // hours = dr["SalesOffice_Hours"].ToString().Replace(",", "<br />");
                //  hours = dr["SalesOffice_Hours"].ToString().Replace("/t", "<br />");
                //hours_locale.Text = hours;
                // Look Up and print the image list for the community when data is available


                //SHARE COMMUNITIY

                String ShareUrl = Convert.ToString(System.Web.HttpContext.Current.Request.Url);

                share_comm.Text += "<a target=\"_blank\" href=\"http://www.facebook.com/sharer/sharer.php?u=" + ShareUrl + "\"><img src=\"http://1627.mobimanage.com/IMAGES/facebook.png\" width=\"31\" height=\"31\" alt=\"Share this Community on Facebook!\" /></a> &nbsp;&nbsp;&nbsp;";

                share_comm.Text += "<a target=\"_blank\" href=\"https://twitter.com/share\" class=\"twitter-share-button\" data-url=\"" + ShareUrl + "\" data-text=\"Check out this community\" data-via=\"fultonhomes\" data-count=\"none\">Tweet</a> &nbsp;&nbsp;&nbsp;" +
                                  "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";

                share_comm.Text += "<a href=\"mailto: ?subject=FULTON Homes Community Information&body=I found this great FULTON Homes Community on their mobile website.   \r\n\r\t\n" + ShareUrl + "\"><img src=\"http://1627.mobimanage.com/IMAGES/mail.png\" width=\"31\" height=\"23\" alt=\"\" /></a>";






                // Handle the drop downs for the community
                //    amenitiesList.Text += dr["Ammenities"];
                //   dirList.Text += dr["Directions"];
                //   schoolList.Text += "District: " + dr["SchoolDistrict"] + "<br />School: " + dr["SchoolName"] + "<br /><a href=\"" + dr["SchoolLink"] + "\">" + dr["SchoolLink"] + "</a>";

                // if (dr["CommunityVideoLink"].ToString().Length == 0 || !dr["CommunityVideoLink"].ToString().Contains("youtube"))
                //   vidContainer.Visible = false;

                //  else
                // {
                //vidList.Text += "<iframe width=\"280\" height=\"210\" src=\"http://www.youtube.com/embed/" + dr["CommunityVideoLink"].ToString().Replace("http://www.youtube.com/watch?v=", "") +
                //     "\" frameborder=\"0\" ></iframe>";
                // }

            }
            catch (Exception)
            {


            }

        }


        protected void MoveIn(object sender, EventArgs e)
        {

            Response.Redirect("/listings.aspx?mir=true&CID=" + subID);


        }




        protected void CallSubDiv(object sender, EventArgs e)
        {

            Response.Redirect("tel://" + SubTel);

        }




    }
}