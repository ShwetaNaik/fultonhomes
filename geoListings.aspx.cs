﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MobileSite
{
    public partial class geoListings : System.Web.UI.Page
    {
        protected int CID = 0;
        protected int listID = 0;
        protected int eID = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (Request.QueryString["redirTo"] != null)
                {
                    if (Request.QueryString["redirTo"].ToString().ToLower() == "detail")
                    {
                        detail.Visible = true;
                    }
                    else
                    {
                        if (Request.QueryString["redirTo"].ToString().ToLower() == "map")
                        {
                            map.Visible = true;
                        }
                        else
                        {
                            list.Visible = true;
                        }
                    }
                }
                else
                {
                    list.Visible = true;
                }

                DisplayData();
            }
        }

        private void DisplayData()
        {

        }
    }
}