﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MobileSite
{
    public partial class search_byLocation : System.Web.UI.Page
    {

        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String listingTable = "FULTON_Comunities";
        protected int listingsPerPage = int.Parse(Utility.GetMobiSiteSettings("numListingRows"));
    



        protected void Page_Load(object sender, EventArgs e)
        {





            if(!IsPostBack) 
            {
             

                if (Request.QueryString["txtlat"] != null && Request.QueryString["txtlon"] != null)
                {


                    Session["txtlat"] = Request.QueryString["txtlat"];
                    Session["txtlon"] = Request.QueryString["txtlon"];
              
                    Response.Redirect("/communityListings.aspx?txtlat=" + Request.QueryString["txtlat"] + "&txtlon=" + Request.QueryString["txtlon"] + "");
                }

                DisplayData();

            }

        }


        protected void DisplayData()
        {
            

               var aSQL = "SELECT DISTINCT SubCity FROM BHI_Subdivision s JOIN  BHI_Builder b ON b.BuilderID = s.BuilderID WHERE b.CORPORATIONID in (SELECT CORPORATIONID FROM Corporation WHERE CorporateBuilderNumber= 'FH101') ORDER BY SubCity";


       DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());
            // Query for listing data
           

            // create a paging system for the data repeater
            PagedDataSource pageable = new PagedDataSource();
            pageable.DataSource = aListingsDS.Tables[0].DefaultView;


            for (int i = 0; i < aListingsDS.Tables[0].Rows.Count; i++)
            {

                DataRow dr = aListingsDS.Tables[0].Rows[i];

                this.cityList.Text += "<a href=\"/CommunityListings.aspx?city=" + dr["SubCity"].ToString() + "\" style=\"text-decoration:none;\"><div class=\"listingMenuItem\" style=\"margin:0px 0px;width:auto;border-radius:0px;-webkit-border-radius:0px;font-size:15pt;\">" + dr["SubCity"].ToString() + "<img src=\"http://1627.mobimanage.com/IMAGES/redarrow.png\" style=\"float:right; display:inline-block;width:10px;margin-top:3px;margin-right:10px;\" /></div></a>";
   

            }
          
        
         this.cityList.Text += "<a href=\"/advanced_search.aspx\" style=\"text-decoration:none;\"><div class=\"listingMenuItem\" style=\"margin:0px 0px;width:auto;border-radius:0px;-webkit-border-radius:0px;font-size:15pt;\">Advanced Search<img src=\"http://1627.mobimanage.com/IMAGES/redarrow.png\" style=\"float:right; display:inline-block;width:10px;margin-top:3px;margin-right:10px;\" /></div></a>";
   
    }

    }
}