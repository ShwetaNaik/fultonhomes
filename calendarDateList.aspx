﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.Master" CodeBehind="calendarDateList.aspx.cs" Inherits="MobileSite.calendarDateList" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">

    <%--- This page displays a list of events based on the date passed in ---%>

    <div class="content">
        <h1 id="pageHeading" runat="server">Calendar</h1>

        <asp:Repeater runat="Server" ID="repGetlist">
            <ItemTemplate>
                <div class="listingContainer">
                    <div style="float: left; min-height: 90px">
                    <%# checkLen(Eval("ImageFile").ToString(), "image") %>
                    </div>
                    <h3>
                        <a href="/calendarDetail.aspx?eID=<%# Eval("EVENTID") %>"><%# Eval("Title") %></a></h3>
                    <%# checkTimes(Eval("startTime").ToString(), Eval("endTime").ToString())%><br />
                    <%# checkLen(Eval("Price").ToString(), "price") %>
                </div>
                <div class="clear">
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Label ID="noEvents" runat="server"></asp:Label>
    </div>

</asp:Content>