﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.Master" CodeBehind="listings.aspx.cs" Inherits="MobileSite.listings" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">
      

 

        <%--- Prev/next and page total ---%>
    <div class="title_bar" style="color:#660000;margin:0px;border-bottom:2px solid #b8b8b8;"><asp:Literal ID="TitleTag" runat="server"></asp:Literal></div>

      
        <div class="pageDataNavContainer" style="display:none;">
            <asp:Label ID="pageTotal" runat="server"></asp:Label>
            <div class="pageDataNav">
            <asp:HyperLink ID="prevBtn" runat="server">Prev</asp:HyperLink><asp:Label ID="div1" runat="server"></asp:Label><asp:HyperLink ID="nextBtn" runat="server">Next</asp:HyperLink>
            </div>
        </div>

        <%--- Main pageable listing repeater ---%>
  
      
            <asp:Literal ID="homeList" runat="server"></asp:Literal>

       <div class="pageDataNavContainer" style="display:none;">
            <div class="pageDataNav">
            <asp:HyperLink ID="prevBtn2" runat="server">Prev</asp:HyperLink><asp:Label ID="div2" runat="server"></asp:Label><asp:HyperLink ID="nextBtn2" runat="server">Next</asp:HyperLink>
            </div>
        </div>

        <form id="Form1" runat="server">  
      <div style="width:100%;text-align:center;">
          <asp:Literal ID="commBtn" runat="server"></asp:Literal>
     </div>
</form>
          

</asp:Content>
