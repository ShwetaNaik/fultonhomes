﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MobileSite
{
    public partial class search_dir : System.Web.UI.Page
    {

        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String listingTable = "FULTON_Comunities";
        protected int listingsPerPage = int.Parse(Utility.GetMobiSiteSettings("numListingRows"));

        String URL = "";
        String aSQL = "";
      
        protected void Page_Load(object sender, EventArgs e)
        {


            if(!IsPostBack) 
            {


           

                if (Request.QueryString["baths"] != null)
                    URL += "&baths=" + Request.QueryString["baths"];
                if (Request.QueryString["maxbaths"] != null)
                    URL += "&maxbaths=" + Request.QueryString["maxbaths"];

                if (Request.QueryString["halfbaths"] != null)
                    URL += "&halfbaths=" + Request.QueryString["halfbaths"];

                if (Request.QueryString["maxhalfbaths"] !=null)
                    URL += "&maxhalfbaths=" + Request.QueryString["maxhalfbaths"];

                if (Request.QueryString["beds"] != null)
                    URL += "&beds=" + Request.QueryString["beds"];

                if (Request.QueryString["maxbeds"] != null)
                    URL += "&maxbeds=" + Request.QueryString["maxbeds"];

                if (Request.QueryString["priceMin"] != null)
                    URL += "&priceMin=" + Request.QueryString["priceMin"];

                if (Request.QueryString["priceMax"] != null)
                    URL += "&priceMax=" + Request.QueryString["priceMax"];

                if (Request.QueryString["stories"] != null)
                    URL += "&stories=" + Request.QueryString["stories"];

                if (Request.QueryString["town"] != null)
                    URL += "&town=" + Request.QueryString["town"];

                if (Request.QueryString["sqft"] != null)
                    URL += "&sqft=" + Request.QueryString["sqft"];

                if (Request.QueryString["maxsqft"] != null)
                    URL += "&maxsqft=" + Request.QueryString["maxsqft"];

           

   
                  DisplayHomes();
        

             DisplayPlans();



             

            }

        }


        protected void DisplayHomes()
        {


         
 

        // if HOMES 

    

               
         


              aSQL = "SELECT e.SpecCity FROM BHI_Specs e JOIN BHI_Plans a ON e.PlanID=a.PlanID JOIN BHI_SubDivision d ON a.SubDivisionID=d.SubDivisionID JOIN BHI_Builder b ON b.BuilderID = d.BuilderID WHERE b.CORPORATIONID in (SELECT CORPORATIONID FROM Corporation WHERE CorporateBuilderNumber= 'FH101') ";

   
                  if (Request.QueryString["stories"] != null)
                      aSQL += " AND E.SpecStories='" + Convert.ToDouble(Request.QueryString["stories"]) + "' ";

                  if (Request.QueryString["town"] != null)
                      aSQL += " AND E.SpecCity ='" + Request.QueryString["town"] + "' ";

                  if (Request.QueryString["priceMin"] != null)
                      aSQL += " AND e.SpecPrice >='" + Convert.ToInt32(Request.QueryString["priceMin"]) + "' ";

                  if (Request.QueryString["priceMax"] != null)
                      aSQL += " AND e.SpecPrice <='" + Convert.ToInt32(Request.QueryString["priceMax"]) + "' ";

                  if (Request.QueryString["sqft"] != null)
                      aSQL += " AND e.SpecSqFt >= '" + Convert.ToInt32(Request.QueryString["sqft"]) + "' ";

                  if (Request.QueryString["maxsqft"] != null)
                      aSQL += " AND e.SpecSqFt <= '" + Convert.ToInt32(Request.QueryString["maxsqft"]) + "' ";

                  if (Request.QueryString["beds"] != null)
                      aSQL += " AND e.SpecBedrooms >= '" + Convert.ToInt32(Request.QueryString["beds"]) + "' ";

                  if (Request.QueryString["maxbeds"] != null)
                      aSQL += " AND e.SpecBedrooms <='" + Convert.ToInt32(Request.QueryString["maxbeds"]) + "' ";

                  if (Request.QueryString["baths"] != null)
                      aSQL += " AND e.SpecBaths >='" + Convert.ToInt32(Request.QueryString["baths"]) + "' ";

                  if (Request.QueryString["maxbaths"] != null)
                      aSQL += " AND e.SpecBaths <='" + Convert.ToInt32(Request.QueryString["maxbaths"]) + "' ";


                  if (Request.QueryString["halfbaths"] != null)
                      aSQL += " AND e.SpecHalfBaths >= '" + Convert.ToInt32(Request.QueryString["halfbaths"]) + "' ";

                  if (Request.QueryString["maxhalfbaths"] != null)
                      aSQL += " AND e.SpecHalfBaths <='" + Convert.ToInt32(Request.QueryString["maxhalfbaths"]) + "' ";


                  int homeCount = 0;

       DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());
            // Query for listing data
           

            // create a paging system for the data repeater
            PagedDataSource pageable = new PagedDataSource();
            pageable.DataSource = aListingsDS.Tables[0].DefaultView;


            for (int i = 0; i < aListingsDS.Tables[0].Rows.Count; i++)
            {




           homeCount = i +1;
               

            }



         this.optionList.Text += "<a href=\"/Listings.aspx?search=homes" + URL + "\" style=\"text-decoration:none;\"><div class=\"listingMenuItem\" style=\"margin:0px 0px;width:auto;border-radius:0px;-webkit-border-radius:0px;font-size:15pt;\">Quick Move-In Homes (" + homeCount.ToString()  +  ") <img src=\"http://1627.mobimanage.com/IMAGES/redarrow.png\" style=\"float:right; display:inline-block;width:10px;margin-top:3px;margin-right:10px;\" /></div></a>";
      
          
  
    }



        protected void DisplayPlans()
        {





            // if HOMES 







            aSQL = "SELECT PlanID FROM BHI_Plans e JOIN  BHI_SubDivision a On e.SubDivisionID=a.SubDivisionID JOIN BHI_Builder b ON b.BuilderID=a.BuilderID WHERE b.CORPORATIONID in (SELECT CORPORATIONID FROM Corporation WHERE CorporateBuilderNumber= 'FH101') ";






            if (Request.QueryString["stories"] != null)

                aSQL += " AND E.Stories='" + Convert.ToDouble(Request.QueryString["stories"]) + "' ";

            if (Request.QueryString["town"] != null)
                aSQL += " AND a.SubCity ='" + Request.QueryString["town"] + "' ";

            if (Request.QueryString["priceMin"] != null)
                aSQL += " AND e.BasePrice >='" + Convert.ToInt32(Request.QueryString["priceMin"]) + "' ";

            if (Request.QueryString["priceMax"] != null)
                aSQL += " AND e.BasePrice <='" + Convert.ToInt32(Request.QueryString["priceMax"]) + "' ";

            if (Request.QueryString["sqft"] != null)
                aSQL += " AND e.BaseSqFt >= '" + Convert.ToInt32(Request.QueryString["sqft"]) + "' ";

            if (Request.QueryString["maxsqft"] != null)
                aSQL += " AND e.BaseSqFt <= '" + Convert.ToInt32(Request.QueryString["maxsqft"]) + "' ";

            if (Request.QueryString["beds"] != null)
                aSQL += " AND e.Bedrooms >= '" + Convert.ToInt32(Request.QueryString["beds"]) + "' ";

            if (Request.QueryString["maxbeds"] != null)
                aSQL += " AND e.Bedrooms <= '" + Convert.ToInt32(Request.QueryString["maxbeds"]) + "' ";

            if (Request.QueryString["baths"] != null)
                aSQL += " AND e.Baths >= '" + Convert.ToInt32(Request.QueryString["baths"]) + "' ";

            if (Request.QueryString["maxbaths"] != null)
                aSQL += " AND e.Baths <= '" + Convert.ToInt32(Request.QueryString["maxbaths"]) + "' ";


            if (Request.QueryString["halfbaths"] != null)
                aSQL += " AND e.HalfBaths >= '" + Convert.ToInt32(Request.QueryString["halfbaths"]) + "' ";

            if (Request.QueryString["maxhalfbaths"] != null)
                aSQL += " AND e.HalfBaths <= '" + Convert.ToInt32(Request.QueryString["maxhalfbaths"]) + "' ";


            int planCount = 0;

            DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());
            // Query for listing data


            // create a paging system for the data repeater
            PagedDataSource pageable = new PagedDataSource();
            pageable.DataSource = aListingsDS.Tables[0].DefaultView;


            for (int i = 0; i < aListingsDS.Tables[0].Rows.Count; i++)
            {


                planCount = i +1;


            }




            this.optionList.Text += "<a href=\"/Listings.aspx?search=plans" + URL + "\" style=\"text-decoration:none;\"><div class=\"listingMenuItem\" style=\"margin:0px 0px;width:auto;border-radius:0px;-webkit-border-radius:0px;font-size:15pt;\">Floor Plans (" + planCount.ToString()  + ") <img src=\"http://1627.mobimanage.com/IMAGES/redarrow.png\" style=\"float:right; display:inline-block;width:10px;margin-top:3px;margin-right:10px;\" /></div></a>";

        }



























    }
}