﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.Master"
    CodeBehind="calendar.aspx.cs" Inherits="MobileSite.calendar" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    
    <div id="jsEnabled2" style="display: none;">
        <div onclick="Javascript:cDrop('byMonth')" class="subNav">By Month<img src="http://lib.mobimanage.com/ICONS/listingArrow.png" style="float: right;" /></div>
    </div>

    <div id="jsDisabled2">
        <a href="<%= ddLink %>" style="text-decoration: none;"><div class="subNav">By Month<img src="http://lib.mobimanage.com/ICONS/listingArrow.png" style="float: right;" /></div></a>
    </div>

    <asp:Label ID="jsEn" runat="server">
        <div id="byMonth" class="subNavDetailContainer" style="display: none;">
    </asp:Label>
    <asp:Label ID="jsDis" runat="server" Visible="false">
        <div id="byMonth" class="subNavDetailContainer">
    </asp:Label>
        
            <asp:Literal ID="monthList" runat="server"></asp:Literal>
    </div>
    <div class="clear"></div>

        <div class="pageDataNavContainer">
            <asp:Label ID="pageTotal" runat="server"></asp:Label>
            <div class="pageDataNav">
            <asp:HyperLink ID="prevBtn" runat="server">Prev</asp:HyperLink><asp:Label ID="div1" runat="server"></asp:Label><asp:HyperLink ID="nextBtn" runat="server">Next</asp:HyperLink>
            </div>
        </div>
        

        <div class="clear"></div>
            <asp:Repeater runAt="Server" id="repGetlist">
                <ItemTemplate>
                <a href="/calendarDetail.aspx?eID=<%# Eval("eventID") %>" style="color: #000; text-decoration: none;">
                    <div class="listingContainer">
                    <div style="float: right; height: auto; padding-top: 35px; vertical-align: middle;"><img src="http://lib.mobimanage.com/ICONS/listingArrow.png" /></div>
                        <%---# checkLen(Eval("image_list").ToString(), "image") ---%>
                        <h2><%# Eval("Title") %></h2>
                        <%# checkDates(Eval("startDate").ToString(), Eval("EndDate").ToString()) %>
                    </div></a>
                    <div class="clear"></div>
                </ItemTemplate>
            </asp:Repeater>  
            <div class="pageDataNavContainer">
            <div class="pageDataNav">
            <asp:HyperLink ID="prevBtn2" runat="server">Prev</asp:HyperLink><asp:Label ID="div2" runat="server"></asp:Label><asp:HyperLink ID="nextBtn2" runat="server">Next</asp:HyperLink>
            </div>
        </div>
    </div>
</asp:Content>