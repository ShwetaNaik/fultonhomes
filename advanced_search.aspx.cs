﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace MobileSite
{
    public partial class advanced_search : System.Web.UI.Page
    {

        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        String listingTable = "BHI_SubDivision";

        protected void Page_Load(object sender, EventArgs e)
        {
           
         //   state.Items.Insert(0, new ListItem("Select State", "0"));
          //  state.SelectedIndex = 0;


            //IOS fix


            if (!IsPostBack)
            {

        

                DisplayData();

            }





        }



        protected void DisplayData()
        {



            //CHECK FOR MOVEIN READY CHECK
            String aSQL = "";


            // Go check the database  FOR SPECS
        //    aSQL = String.Format("SELECT Distinct(a.SubState), a.SubCity, a.SubCounty,a.SubDivisionName,e.PlanName FROM BHI_SubDivision a Inner Join BHI_Plans e ON a.SubDivisionID=e.SubDivisionID"
             //     + " JOIN BHI_Builder b ON b.BuilderID=a.BuilderID"
              //    + " WHERE B.BUILDERID IN (SELECT DISTINCT BuilderID FROM Builder"
              //    + " WHERE CORPORATIONID IN (SELECT DISTINCT CORPORATIONID"
              //    + " FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101')) ORDER BY a.SubState", listingTable);


            // Go check the database  FOR SPECS
            aSQL = String.Format("SELECT Distinct(a.SubCity) FROM BHI_SubDivision a Inner Join BHI_Plans e ON a.SubDivisionID=e.SubDivisionID"
                  + " JOIN BHI_Builder b ON b.BuilderID=a.BuilderID"
                  + " WHERE B.BUILDERID IN (SELECT DISTINCT BuilderID FROM Builder"
                  + " WHERE CORPORATIONID IN (SELECT DISTINCT CORPORATIONID"
                  + " FROM Corporation WHERE CORPORATEBUILDERNUMBER = 'FH101')) ORDER BY a.SubCity", listingTable);

            DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, aSQL.ToString());


            this.town.Items.Clear();

            this.town.Items.Insert(0, "All Cities");



            DataRow dr = aListingsDS.Tables[0].Rows[0];

            for (int i = 0; i < aListingsDS.Tables[0].Rows.Count; i++) 
           {

               this.town.Items.Add(aListingsDS.Tables[0].Rows[i]["SubCity"].ToString());


           }





        }














        protected void addCity(object sender, EventArgs e)
        { 
            this.town.Items.Insert(0, "City");
        }


       






        protected void results_Click(object sender, EventArgs e)
        {


            String URL = "/search_dir.aspx?t=1";

            if (this.baths.SelectedIndex > 0 ) 
                URL += "&baths=" + this.baths.SelectedValue;
            if (this.maxbaths.SelectedIndex > 0)
                URL += "&maxbaths=" + this.maxbaths.SelectedValue;

            if (this.halfbaths.SelectedIndex > 0)
                URL += "&halfbaths=" + this.halfbaths.SelectedValue;
            if (this.maxhalfbaths.SelectedIndex > 0)

                URL += "&maxhalfbaths=" + this.maxhalfbaths.SelectedValue;

            if (this.beds.SelectedIndex > 0) 
                URL += "&beds=" + this.beds.SelectedValue;
            if (this.maxbeds.SelectedIndex > 0)
                URL += "&maxbeds=" + this.maxbeds.SelectedValue;

        


            if (this.pmin.SelectedIndex > 0)
                URL += "&priceMin=" + this.pmin.SelectedValue;
            if (this.pmax.SelectedIndex > 0)
                URL += "&priceMax=" + this.pmax.SelectedValue;

            if (this.stories.SelectedIndex > 0)
                URL += "&stories=" + this.stories.SelectedValue;


            if (this.town.SelectedIndex > 0)
                URL += "&town=" + this.town.SelectedValue;

            if (this.sqft.SelectedIndex > 0)
                URL += "&sqft=" + this.sqft.SelectedValue;
            if (this.maxsqft.SelectedIndex > 0)
                URL += "&maxsqft=" + this.maxsqft.SelectedValue;


            URL += "&MIR=TRUE";
        
            Response.Redirect(URL.ToString());


        }


        protected void resetForm(object sender, EventArgs e)
        {

   
            this.beds.ClearSelection();
            this.baths.ClearSelection();
            this.maxbeds.ClearSelection();
            this.maxbaths.ClearSelection();
            this.sqft.ClearSelection();
            this.stories.ClearSelection();
            this.maxsqft.ClearSelection();
            this.pmax.ClearSelection();
            this.pmin.ClearSelection();
            this.halfbaths.ClearSelection();
            this.maxhalfbaths.ClearSelection();
       
     

        }

      






    }
}