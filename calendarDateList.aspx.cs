﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MobileSite
{
    public partial class calendarDateList : System.Web.UI.Page
    {
        protected static String connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DataConn"].ToString();
        protected String eventsTable = Utility.GetMobiSiteSettings("eventsTable");
        protected int listingsPerPage = int.Parse(Utility.GetMobiSiteSettings("numListingRows"));
        String month = "";
        String day = "";
        String year = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["month"] != null)
                    month = Request.QueryString["month"];
                if (Request.QueryString["year"] != null)
                {
                    year = Request.QueryString["year"];
                }
                if (Request.QueryString["day"] != null)
                    day = Request.QueryString["day"];
                if (month.Length == 0)
                    month = DateTime.Now.Month.ToString();
                if (year.Length == 0)
                    year = DateTime.Now.Year.ToString();
                if (day.Length == 0)
                    day = DateTime.Now.Day.ToString();
                DisplayData();
            }
        }

        private void DisplayData()
        {
            //Query to get all events on selected day
            if (month.Length > 0 && day.Length > 0 && year.Length > 0)
            {
                pageHeading.InnerText = "Events on " + month + "/" + day + "/" + year;

                String qry = "SELECT EVENTID, TITLE, DESCRIPTION, STARTDATE, ENDDATE, STARTTIME, ENDTIME, REGISTRATIONURL, ImageFile, PRICE FROM " + eventsTable + " WHERE STARTDATE <= '"
                + month + "/" + day + "/" + year + "' AND ENDDATE >= '" + month + "/" + day + "/" + year + "'";

                DataSet aListingsDS = SqlHelper.ExecuteDataset(connStr, CommandType.Text, qry);

                if (aListingsDS.Tables[0].Rows.Count > 0)
                {
                    repGetlist.DataSource = aListingsDS.Tables[0];
                    repGetlist.DataBind();
                }
                else
                {
                    noEvents.Text = "There are no events scheduled on the selected day. Please go <a href='" + Request.UrlReferrer + "'>back</a> and choose a different day.";
                }

            }
            else
            {
                noEvents.Text = "There are no events scheduled on the selected day. Please go <a href='" + Request.UrlReferrer + "'>back</a> and choose a different day.";
            }
        }

        public String checkDates(String start, String end)
        {
            DateTime startT = DateTime.Parse(start);
            DateTime endT = DateTime.Parse(end);

            if (startT == endT)
                return startT.ToString("d");
            return null;
        }

        public String checkTimes(String start, String end)
        {
            DateTime startT = DateTime.Parse(start);
            DateTime endT = DateTime.Parse(end);

            if (startT == endT)
                return start;

                return start + " - " + end;

        }

        public String checkLen(String input, String type)
        {
            switch (type.ToLower())
            {
                case "price":
                    if (input.Length > 0)
                        return "Price: " + input;
                    else
                        return null;
                case "image":
                    if (input.Length > 0)
                        return "<img src='" + input + "'style='vertical-align: top; max-height: 95px; width: 90px;' />";
                    else
                        return null;
                default:
                    return input;
            }
            

        }
    }
}