﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Data;
using System.Net.Mail;

namespace MobileSite
{
    internal static class Utility
    {
        /*
        Pattern	            Description
        ^	                Start at the beginning of the string.
        \s*	                Match zero or more white-space characters.
        [\+-]?	            Match zero or one occurrence of either the positive sign or the negative sign.
        \s?	                Match zero or one white-space character.
        \$?	                Match zero or one occurrence of the dollar sign.
        \s?	                Match zero or one white-space character.
        \d*	                Match zero or more decimal digits.
        \.?	                Match zero or one decimal point symbol.
        \d{2}?	            Match two decimal digits zero or one time.
        (\d*\.?\d{2}?){1}	Match the pattern of integral and fractional digits separated by a decimal point symbol at least one time.
        $	                Match the end of the string.
        */

        internal static String MobileHTMLCleanse(String pData)
        {
            String aResult = pData;
            String aPattern;
            Regex aRegEx;
            MatchCollection aMatches;

            String aPattern2;
            Regex aRegEx2;
            MatchCollection aMatches2;

            //        <!--- REPLACE INVALID TAGS WITH APPROPRIATE TAGS---->	
            //<!--- REMOVE CRAZY ATTRIBUTES THAT .MOBI DOESN'T LIKE ---->
            aResult = Regex.Replace(aResult, "dir=\"[A-Za-z0-9]*\"", "");
            aResult = Regex.Replace(aResult, "align=\"[A-Za-z0-9]*\"", "");
            aResult = Regex.Replace(aResult, "sponsored=\"[A-Za-z0-9]*\"", "");
            aResult = Regex.Replace(aResult, "border=\"[A-Za-z0-9]*\"", "");

            //<cfset npageData = replacenocase(npageData,'<B>','<strong>','ALL')>
            //<cfset npageData = replacenocase(npageData,'</B>','</strong>','ALL')>
            //<cfset npageData = replacenocase(npageData,' & ',' &amp; ','ALL')>
            aResult = aResult.Replace("<b>", "<strong>");
            aResult = aResult.Replace("</b>", "</strong>");
            aResult = aResult.Replace(" & ", " &amp; ");

            //<cfset npageData = replacenocase(npageData,'<P>','<div style="margin-top:12px;">','ALL')>
            //<cfset npageData = replacenocase(npageData,'</P>','</div>','ALL')>
            aResult = aResult.Replace("<p>", "<div style=\"margin-top:12px;\">");
            aResult = aResult.Replace("</p>", "</div>");

            //<!--- change all spans to divs --->
            //<cfset npageData = rereplacenocase(npageData,'<span','<div','ALL')>
            //<cfset npageData = rereplacenocase(npageData,'</span','</div','ALL')>
            aResult = aResult.Replace("<span", "<div");
            aResult = aResult.Replace("</span", "</div>");

            //<!---- REMOVE SELF CLOSES TO KEEP MULTIPLE / FROM OCCURING ---->
            String oldImage = "";
            String newImage = "";
            aPattern = "(<(IMG|BR)[^>]*/>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = aResult.Substring(aMatch.Index, aMatch.Length).Replace("/>", ">");
                    aResult = aResult.Replace(oldImage, newImage);
                }
            }


            //<!--- CONVERT ALL TAGS TO LOWERCASE --->
            //myRE = "(<[^>]*>)";
            aPattern = "(<[^>]*>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = aResult.Substring(aMatch.Index, aMatch.Length).ToLower();

                    aPattern2 = "youtube";
                    aRegEx2 = new Regex(aPattern2, RegexOptions.IgnoreCase);
                    aMatches2 = aRegEx2.Matches(newImage, aMatch.Index);
                    if (aMatches2.Count > 0)
                    {
                    }
                    else
                    {
                        aPattern2 = "youtu.be";
                        aRegEx2 = new Regex(aPattern2, RegexOptions.IgnoreCase);
                        aMatches2 = aRegEx2.Matches(newImage, aMatch.Index);
                        if (aMatches2.Count > 0)
                        {
                        }
                        else
                        {
                            aResult = aResult.Replace(oldImage, newImage);
                        }
                    }

                }
            }

            //<!--- FIND ALL IMAGES WITHOUT ALT TAGS AND ADD THEM--->
            aPattern = "(<(IMG)[^>]*>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    if (!oldImage.Contains("alt="))
                    {
                        newImage = aResult.Substring(aMatch.Index, aMatch.Length).Replace(">", " alt='' >");
                        aResult = aResult.Replace(oldImage, newImage);
                    }
                }
            }


            //<!--- FIND ALL IMAGES WITH ALT TAGS THAT ARE MISSING ""--->
            //// FIND ALL ALT TAGS THAT DO NOT HAVE " AFTER alt=, STOP AT THE NEXT ATTRIBUTE
            aPattern = "(<*alt=[^\"]*[a-zA-Z0-9]*[a-zA-Z0-9][=])";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    aPattern2 = "([^alt=][a-zA-Z0-9>][=>])";
                    aRegEx2 = new Regex(aPattern2, RegexOptions.IgnoreCase);
                    aMatches2 = aRegEx2.Matches(aResult, aMatch.Index);
                    if (aMatches2.Count > 0)
                    {
                        foreach (Match aMatch2 in aMatches2)
                        {
                            oldImage = aResult.Substring(aMatch.Index, aMatch2.Length - aMatch.Length - 2);
                            newImage = oldImage + "\"";
                            aResult = aResult.Replace(oldImage, newImage);
                        }
                    }
                }
            }

            //<CFSCRIPT>
            //myRE = '(<*alt=[^"]*[a-zA-Z0-9]["])';
            aPattern = "(<*alt=[^\"]*[a-zA-Z0-9][\"])";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = aResult.Substring(aMatch.Index, aMatch.Length).Replace("alt=", "alt=\"");
                    aResult = aResult.Replace(oldImage, newImage);
                }
            }

            //<!--- REMOVE <LINK TAGS --->
            //myRE = "(<link[^>]*>)";
            aPattern = "(<link[^>]*>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = "";
                    aResult = aResult.Replace(oldImage, newImage);
                }
            }

            //<!--- REMOVE ?XML TAGS --->
            //myRE = "(<[?]xml[^>]*>)";
            aPattern = "(<[?]xml[^>]*>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = "";
                    aResult = aResult.Replace(oldImage, newImage);
                }
            }

            //<!--- REMOVE ALL SKYPE TAGS --->
            //myRE = "(<skype[^>]*>)";
            aPattern = "(<skype[^>]*>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = "";
                    aResult = aResult.Replace(oldImage, newImage);
                }
            }

            //myRE = "(</skype[^>]*>)";
            aPattern = "(</skype[^>]*>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = "";
                    aResult = aResult.Replace(oldImage, newImage);
                }
            }

            //<!--- REMOVE ALL TITLE TAGS --->
            //myRE = "(<title[^>]*>)";
            aPattern = "(<title[^>]*>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = "";
                    aResult = aResult.Replace(oldImage, newImage);
                }
            }
            //myRE = "(</title[^>]*>)";
            aPattern = "(</title[^>]*>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = "";
                    aResult = aResult.Replace(oldImage, newImage);
                }
            }

            //<!---- CLOSE ALL TAGS THAT CAN SELFCLOSE ---->
            //myRE = "(<(IMG|BR)[^>]*>)";
            aPattern = "(<(IMG|BR)[^>]*>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = (aResult.Substring(aMatch.Index, aMatch.Length).Replace(">", "/>")).Replace("//", "/>");
                    aResult = aResult.Replace(oldImage, newImage);
                }
            }

            //<!--- REMOVE ALL FONT TAGS --->
            //myRE = "(<font[^>]*>)";
            aPattern = "(<font[^>]*>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = "";
                    aResult = aResult.Replace(oldImage, newImage);
                }
            }

            //myRE = "(</font[^>]*>)";
            aPattern = "(</font[^>]*>)";
            aRegEx = new Regex(aPattern, RegexOptions.IgnoreCase);
            aMatches = aRegEx.Matches(aResult);
            if (aMatches.Count > 0)
            {
                foreach (Match aMatch in aMatches)
                {
                    oldImage = aResult.Substring(aMatch.Index, aMatch.Length);
                    newImage = "";
                    aResult = aResult.Replace(oldImage, newImage);
                }
            }

            return aResult;
        }
        internal static String GetSiteSettings(String pKey)
        {
            object aResult = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.ConnectionStrings["PortalConn"].ToString(), CommandType.Text, String.Format("SELECT [Value] FROM mobiSettings WHERE [Key] = '{0}'", pKey));
            if (aResult != null)
                return aResult.ToString();
            else
                return "";
        }
        internal static String GetMobiSiteSettings(String pKey)
        {
            object aResult = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.ConnectionStrings["PortalConn"].ToString(), CommandType.Text, String.Format("SELECT {0} FROM mobiSiteSettings WHERE DomainID = {1}", pKey, System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString()));
            if (aResult != null)
                return aResult.ToString();
            else
                return "";
        }
        internal static Double GetMobiSiteSettingsDbl(String pKey)
        {
            object aResult = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.ConnectionStrings["PortalConn"].ToString(), CommandType.Text, String.Format("SELECT {0} FROM mobiSiteSettings WHERE DomainID = {1}", pKey, System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString()));
            if (aResult != null)
                return Convert.ToDouble(aResult);
            else
                return 0;
        }
        internal static Int32 GetMobiSiteSettingsInt(String pKey)
        {
            object aResult = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.ConnectionStrings["PortalConn"].ToString(), CommandType.Text, String.Format("SELECT {0} FROM mobiSiteSettings WHERE DomainID = {1}", pKey, System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString()));
            if (aResult != null)
                return Convert.ToInt32(aResult);
            else
                return 0;
        }
        internal static String GetDomainSettings(String pKey)
        {
            object aResult = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.ConnectionStrings["DBConn"].ToString(), CommandType.Text, String.Format("SELECT {0} FROM [Domains] WHERE DomainID = {1}", pKey, System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString()));
            if (aResult != null)
                return aResult.ToString();
            else
                return "";
        }
        internal static String GetCustomerSettings(String pKey)
        {
            try
            {
                object aResult = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.ConnectionStrings["PortalConn"].ToString(), CommandType.Text, String.Format("SELECT {0} FROM uMobi_Customers WHERE DomainID = {1}", pKey, System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString()));
                if (aResult != null)
                    return aResult.ToString();
                else
                    return "";
            }
            catch { return ""; }
        }
        internal static Boolean GetCustomerSettingsBoolean(String pKey)
        {
            try
            {
                object aResult = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.ConnectionStrings["PortalConn"].ToString(), CommandType.Text, String.Format("SELECT {0} FROM uMobi_Customers WHERE DomainID = {1}", pKey, System.Configuration.ConfigurationManager.AppSettings["DomainID"].ToString()));
                if (aResult != null)
                    return Convert.ToBoolean(aResult);
                else
                    return false;
            }
            catch { return false; }
        }
        internal static void SendEmail(String pFrom, String pTo, String pSubject, String pMessage, String pBccEmail)
        {
            try
            {
                MailMessage aMail = new MailMessage();
                aMail.IsBodyHtml = false;
                aMail.Subject = pSubject;
                aMail.Body = pMessage;
                aMail.BodyEncoding = System.Text.Encoding.UTF8;
                if (pFrom.Length == 0)
                    pFrom = GetSiteSettings("SMTP_Username");
                MailAddress aFromAddress = new MailAddress(pFrom);
                MailAddress aToAddress = new MailAddress(pTo);
                if (pBccEmail.Length > 0)
                {
                    MailAddress aBcc = new MailAddress(pBccEmail);
                    aMail.Bcc.Add(aBcc);
                }
                aMail.From = aFromAddress;
                aMail.To.Add(aToAddress);

                SmtpClient aClient;
                String aServer = GetSiteSettings("SMTP_Server");
                String aPort = GetSiteSettings("SMTP_Port");
                String aUsername = GetSiteSettings("SMTP_Username");
                String aPassword = GetSiteSettings("SMTP_Password");
                if (aPort.Length > 0)
                    aClient = new SmtpClient(aServer, Convert.ToInt32(aPort));
                else
                    aClient = new SmtpClient(aServer);

                aClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                aClient.EnableSsl = false;
                System.Net.NetworkCredential cCredential;
                if (aPassword.Length > 0)
                    cCredential = new System.Net.NetworkCredential(aUsername, aPassword);
                else
                    cCredential = new System.Net.NetworkCredential();

                aClient.Credentials = cCredential;


                aClient.Send(aMail);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }
    }
}