﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.Master" CodeBehind="Deals.aspx.cs" Inherits="MobileSite.Deals" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
 
        <div class="pageDataNavContainer">
            <asp:Label ID="pageTotal" runat="server"></asp:Label>
            <div class="pageDataNav">
            <asp:HyperLink ID="prevBtn" runat="server">Prev</asp:HyperLink><asp:Label ID="div1" runat="server"></asp:Label><asp:HyperLink ID="nextBtn" runat="server">Next</asp:HyperLink>
            </div>
        </div>
        

        <div class="clear"></div>
            <asp:Repeater runAt="Server" id="repGetlist">
                <ItemTemplate>
                <a href="/dealsDetail.aspx?oID=<%# Eval("offerID") %>" style="color: #000; text-decoration: none;">
                    <div class="listingContainer">
                    <div style="float: right; height: auto; padding-top: 15px; vertical-align: middle;"><img src="http://lib.mobimanage.com/ICONS/listingArrow.png" /></div>
                        <%---# checkLen(Eval("image_list").ToString(), "image") ---%>
                        <h2><%# Eval("Title") %></h2>
                        <%# Eval("Company") %><br />
                        <%# printDate(Eval("RedeemStart").ToString(), Eval("RedeemEnd").ToString()) %>
                    </div></a>
                </ItemTemplate>
            </asp:Repeater>  
            <div class="pageDataNavContainer">
            <div class="pageDataNav">
            <asp:HyperLink ID="prevBtn2" runat="server">Prev</asp:HyperLink><asp:Label ID="div2" runat="server"></asp:Label><asp:HyperLink ID="nextBtn2" runat="server">Next</asp:HyperLink>
            </div>
        </div>
    </div>

</asp:Content>