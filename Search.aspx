﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.Master" CodeBehind="Search.aspx.cs" Inherits="MobileSite.Search" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <div class="contentOuter">
        <div class="content">
            <h1>Search</h1>
            <hr />
            <form action="/Search.aspx" method="get">
            <div style="text-align: center; display: block; width: auto;">
            <input type="text" name="listSrch" style="width: 200px; font-size: 16px; line-height: 16px;" /><input type="submit" value="Go" style="line-height: 18px;" />
            </div>
        </form>
        </div>
        <%--- Prev/next and page total ---%>
        <div class="clear"></div>
        <div class="pageDataNavContainer">
            <asp:Label ID="pageTotal" runat="server"></asp:Label>
            <div class="pageDataNav">
            <asp:HyperLink ID="prevBtn" runat="server">Prev</asp:HyperLink><asp:Label ID="div1" runat="server"></asp:Label><asp:HyperLink ID="nextBtn" runat="server">Next</asp:HyperLink>
            </div>
        </div>

        <%--- Main pageable listing repeater ---%>
        <div class="clear"></div>
            <asp:Repeater runAt="Server" id="repGetlist">
                <ItemTemplate>
                <a href="/listingDetail.aspx?CNM=<%# Request.QueryString["CNM"] %>&listID=<%# Eval("listingID") %>" style="color: #000; text-decoration: none;">
                    <div class="listingContainer">
                        <%# checkLen(Eval("image_list").ToString(), "image") %>
                        <h2><%# Eval("Company") %></h2>
                        <%# checkLen(Eval("address1").ToString(), "Address") %>
                        <%# checkLen(Eval("city").ToString(), "City") %> <%# Eval("state")%> <%# Eval("Zip") %>
                    </div></a>
                    <div class="clear"></div>
                </ItemTemplate>
            </asp:Repeater>  
            <div class="pageDataNavContainer">
            <div class="pageDataNav">
            <asp:HyperLink ID="prevBtn2" runat="server">Prev</asp:HyperLink><asp:Label ID="div2" runat="server"></asp:Label><asp:HyperLink ID="nextBtn2" runat="server">Next</asp:HyperLink>
            </div>
        </div>
    </div>
</asp:Content>
