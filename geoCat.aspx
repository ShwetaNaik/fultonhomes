﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Navigation.Master" CodeBehind="geoCat.aspx.cs" Inherits="MobileSite.geoCat" %>

<asp:Content ID="main" ContentPlaceHolderID="cphMain" runat="server">
    <asp:Literal runat="server" ID="subPages"></asp:Literal>
    <div class="content"><h1>NEARBY</h1>
            <hr /></div>
         
            <div class="geoNav">
            <form method="get" action="/geoListings.aspx" enctype="multipart/form-data" lang="en">
            <input type="hidden" id="geoPost1" name="geoPost" value="1" />
            <asp:Label ID="txtxlatLine1" runat="server"></asp:Label>
            <asp:Label ID="txtLongLine1" runat="server"></asp:Label>
                <script type="text/javascript" defer="defer">
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                        function (position) {
                            document.getElementById("txtLat1").value = position.coords.latitude;
                            document.getElementById("txtLong1").value = position.coords.longitude;
                        }, function (error) { }, { enableHighAccuracy: true }
                    );
                    }
                    else {

                    } 
	
                </script>
                <input type="hidden" id="CID1" name="CID" value="397" />
                <input type="hidden" id="CNM1" name="CNM" value="Slumber" />
                <input type="submit" value="Slumber"/>
                
            </form>
            </div>
            <div class="geoNav">
            <form method="get" action="/geoListings.aspx" enctype="multipart/form-data" lang="en">
            <input type="hidden" id="geoPost2" name="geoPost" value="1" />
            <asp:Label ID="txtxlatLine2" runat="server"></asp:Label>
            <asp:Label ID="txtLongLine2" runat="server"></asp:Label>
                <script type="text/javascript" defer="defer">
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                        function (position) {
                            document.getElementById("txtLat2").value = position.coords.latitude;
                            document.getElementById("txtLong2").value = position.coords.longitude;
                        }, function (error) { }, { enableHighAccuracy: true }
                    );
                    }
                    else {

                    } 
	
                </script>
                <input type="hidden" id="CID2" name="CID" value="398" />
                <input type="hidden" id="CNM2" name="CNM" value="Frolic" />
                <input type="submit" value="Frolic"/>
               

            </form>
            </div>
            <div class="geoNav">
            <form method="get" action="/geoListings.aspx" enctype="multipart/form-data" lang="en">
            <input type="hidden" id="geoPost3" name="geoPost" value="1" />
            <asp:Label ID="txtxlatLine3" runat="server"></asp:Label>
            <asp:Label ID="txtLongLine3" runat="server"></asp:Label>
                <script type="text/javascript" defer="defer">
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                        function (position) {
                            document.getElementById("txtLat3").value = position.coords.latitude;
                            document.getElementById("txtLong3").value = position.coords.longitude;
                        }, function (error) { }, { enableHighAccuracy: true }
                    );
                    }
                    else {

                    } 
	
                </script>
                <input type="hidden" id="CID3" name="CID" value="395" />
                <input type="hidden" id="CNM3" name="CNM" value="Feast" />
                <input type="submit" value="Feast"/>

            </form>
            </div>
            <div class="geoNav">
            <form method="get" action="/geoListings.aspx" enctype="multipart/form-data" lang="en">
            <input type="hidden" id="geoPost4" name="geoPost" value="1" />
            <asp:Label ID="txtxlatLine4" runat="server"></asp:Label>
            <asp:Label ID="txtLongLine4" runat="server"></asp:Label>
                <script type="text/javascript" defer="defer">
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                        function (position) {
                            document.getElementById("txtLat4").value = position.coords.latitude;
                            document.getElementById("txtLong4").value = position.coords.longitude;
                        }, function (error) { }, { enableHighAccuracy: true }
                    );
                    }
                    else {

                    } 
	
                </script>
                <input type="hidden" id="CID4" name="CID" value="399" />
                <input type="hidden" id="CNM4" name="CNM" value="Shop" />
                <input type="submit" value="Shop"/>

            </form>
            </div>

</asp:Content>
